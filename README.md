# Material Design Navigation Drawer

App sample about how to implement the [Navigation Drawer following the Material Design guidelines](https://goo.gl/qpKNsR).

This implementation is older that the [Design Support Library](http://goo.gl/GgLTjB) so does NOT use it.
For an implementation using the [Navigation View](https://goo.gl/XwIo9D) from the [Design Support Library](http://goo.gl/GgLTjB) check out [this branch](https://goo.gl/861g0S)

Check out these articles for further information.

1. [Navigation Drawer: Sizing](http://goo.gl/Zc3kMT)
2. [Navigation Drawer: Styling](http://goo.gl/rTS3MF)
3. [Navigation Drawer: Navigating](https://goo.gl/wjT568)

## License

The contents of this repository are covered under the [MIT License](LICENSE).
