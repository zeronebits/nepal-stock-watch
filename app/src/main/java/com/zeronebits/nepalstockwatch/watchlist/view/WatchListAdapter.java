package com.zeronebits.nepalstockwatch.watchlist.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.watchlist.model.WatchListUserModel;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Own on 11/11/2017.
 */

public class WatchListAdapter extends RecyclerView.Adapter<WatchListAdapter.MyViewHolder> {

    public ArrayList<WatchListUserModel> watchListUserModels;
    Context context;
    WatchListView watchListView;
    public WatchListAdapter(ArrayList<WatchListUserModel> watchListUserModels, Context context,WatchListView watchListView) {
        this.context = context;
        this.watchListUserModels = watchListUserModels;
        this.watchListView = watchListView;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.watch_list_single_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final WatchListUserModel watchListModel = watchListUserModels.get(position);
        holder.closing.setText("Closing: "+watchListModel.getcLOSE());
        holder.diff.setText("Diff: "+watchListModel.getdIFF());
        holder.symbol.setText(watchListModel.getpRFLCODE());
        holder.symbolName.setText(watchListModel.getpRFLCODE());
        holder.vol.setText("Vol: "+watchListModel.getvOLUME()+"");
        holder.high.setText("High: "+watchListModel.gethIGH()+"");
        holder.low.setText("Low: "+watchListModel.getlOW()+"");

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                watchListView.deleteWatchList(watchListModel.getpRFLCODE());
            }
        });
    }

    @Override
    public int getItemCount() {
        return watchListUserModels.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_symbol_name)
        TextView symbol;
        @BindView(R.id.tv_company_name)
        TextView symbolName;
        @BindView(R.id.tv_diff)
        TextView diff;
        @BindView(R.id.tv_closing)
        TextView closing;
        @BindView(R.id.tv_vol)
        TextView vol;
        @BindView(R.id.tv_ltp)
        TextView ltp;
        @BindView(R.id.tv_high)
        TextView high;
        @BindView(R.id.tv_low)
        TextView low;
        @BindView(R.id.iv_cart_delete)
        LinearLayout delete;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
