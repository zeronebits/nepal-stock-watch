package com.zeronebits.nepalstockwatch.watchlist.model;

import io.realm.RealmObject;

/**
 * Created by Own on 11/13/2017.
 */

public class WatchListModelDatabase extends RealmObject {

    String companySymbol;


    public String getCompanySymbol() {
        return companySymbol;
    }

    public void setCompanySymbol(String companySymbol) {
        this.companySymbol = companySymbol;
    }
}
