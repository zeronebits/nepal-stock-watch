package com.zeronebits.nepalstockwatch.watchlist.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.helper.LoadingHelper;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.login.view.LoginActivity;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;
import com.zeronebits.nepalstockwatch.watchlist.model.WatchListModel;
import com.zeronebits.nepalstockwatch.watchlist.model.WatchListParser;
import com.zeronebits.nepalstockwatch.watchlist.view.WatchListView;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 10/23/2017.
 */

public class WatchListImp implements WatchListPresenter{

    WatchListView watchListView;
    Context context;

    public WatchListImp(WatchListView watchListView, Context context) {
        this.watchListView = watchListView;
        this.context = context;
    }


    @Override
    public void addWatchList(WatchListModel watchListModel) {
        final LoadingHelper loadingHelper = new LoadingHelper(context);
        loadingHelper.showDialog();
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.addWatchList(watchListModel);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  ");
                if(loadingHelper.isShowing()){
                    loadingHelper.disMiss();
                }
                if (response.isSuccessful()) {
                    try {
                        Log.d("sdkfjdsfsdf", response.body().string());
                        getWatchList(Constant.USERCODE);
                        Toast.makeText(context, "Added", Toast.LENGTH_SHORT).show();
//                        splashView.verifySucess()
                    } catch (IOException e) {
                        e.printStackTrace();
                    }/* catch (JSONException e) {
                    e.printStackTrace();
                }
*/
                } else if(response.code() == 500) {
                    Toast.makeText(context, "Already Added", Toast.LENGTH_SHORT).show();

                }else{
                    try {
                        Log.d("dkfhsdjfsdfds",response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(loadingHelper.isShowing()){
                    loadingHelper.disMiss();
                }
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getWatchList(String usercode) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Log.d("dfjdfsdf",Constant.ACESS_TOKEN);
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Log.d("sajdsadsad",Constant.USERCODE);
        Call<ResponseBody> getCountry = loginRegInterface.getWatchList(usercode);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  "+response.raw().request().url());

                if (response.isSuccessful()) {
                    try {
                        WatchListParser watchListParser = new WatchListParser(response.body().string());
                        watchListParser.parser();
                        watchListView.userWatchList(watchListParser.watchListUserModels);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                    e.printStackTrace();
                }

                } else if(response.code() == 422) {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    watchListView.emptyWatchList();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void deleteWatchList(String s) {
        final LoadingHelper loadingHelper = new LoadingHelper(context);
        loadingHelper.showDialog();
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.deleteWatchList(s);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  "+response.raw().request().url());
                if(loadingHelper.isShowing()){
                    loadingHelper.disMiss();
                }
                if (response.isSuccessful()) {
                    try {
                        watchListView.deleteSucess();
                        Log.d("fsdjfhsdfsdf",response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else if(response.code() == 422) {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    watchListView.emptyWatchList();
                }else {
                    watchListView.emptyWatchList();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }
}
