package com.zeronebits.nepalstockwatch.watchlist.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Own on 10/23/2017.
 */

public class WatchListModel {

    @SerializedName("PRFL_CODE")
    String companyName;
    @SerializedName("CSTM_CODE")
    String custCode;

    public WatchListModel(String companyName, String custCode) {
        this.companyName = companyName;
        this.custCode = custCode;
    }
}
