package com.zeronebits.nepalstockwatch.indices.view;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.data.Entry;
import com.inqbarna.tablefixheaders.TableFixHeaders;
import com.inqbarna.tablefixheaders.adapters.BaseTableAdapter;
import com.wang.avi.AVLoadingIndicatorView;
import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.dashboard.model.MarketSummaryModel;
import com.zeronebits.nepalstockwatch.dashboard.presentar.DashBoardTableImplementar;
import com.zeronebits.nepalstockwatch.dashboard.presentar.DashBoardTablePresenter;
import com.zeronebits.nepalstockwatch.dashboard.tabs.view.Company;
import com.zeronebits.nepalstockwatch.dashboard.tabs.view.Index;
import com.zeronebits.nepalstockwatch.dashboard.tabs.view.SubIndex;
import com.zeronebits.nepalstockwatch.dashboard.view.DashBoardTableAdapter;
import com.zeronebits.nepalstockwatch.dashboard.view.DashBoardView;
import com.zeronebits.nepalstockwatch.portfolio.view.PorfolioTabAdapter;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Own on 11/11/2017.
 */

public class Indices extends BaseFragment implements DashBoardView {

    DashBoardTablePresenter dashBoardTablePresenter;
    @BindView(R.id.table_transaction)
    TableFixHeaders tableTransaction;
    @BindView(R.id.table_shared_trade)
    TableFixHeaders tableSharedTrade;
    @BindView(R.id.table_turn_over)
    TableFixHeaders tableTurnOver;
    @BindView(R.id.table_gain)
    TableFixHeaders tableGain;
    @BindView(R.id.table_loss)
    TableFixHeaders tableLoss;
    @BindView(R.id.avi)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.ll_dashboard_table)
    LinearLayout dashboardTable;

    MainActivity mainActivity;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.pager)
    ViewPager viewPager;
    public ArrayList<String> indexList;

    @BindView(R.id.tv_turnover)
    TextView turnOver;
    @BindView(R.id.tv_total_traded_shares)
    TextView tradedShares;
    @BindView(R.id.tv_transaction)
    TextView transaction;
    @BindView(R.id.tv_total_scrips_traded)
    TextView totalScips;
    @BindView(R.id.tv_total_market_capitalization)
    TextView totalMarket;
    @BindView(R.id.tv_flated_market_capitalzation)
    TextView flatedMarket;

    @Override
    protected int getLayout() {
        return R.layout.dashboard;
    }

    @Override
    protected void init(View view) {
        tabs.setupWithViewPager(viewPager);
        createViewPager(viewPager);
        createTabIcons();
        loadingIndicatorView.setVisibility(View.VISIBLE);
        dashboardTable.setVisibility(View.GONE);
        dashBoardTablePresenter = new DashBoardTableImplementar(this, getActivity());
        dashBoardTablePresenter.getAllDashboardData();

        hideKeyboard();
    }


    @Override
    public void onResume() {
        super.onResume();
        try {
            mainActivity = (MainActivity) getActivity();
            mainActivity.setToolBarTitle("Indices");
        } catch (Exception e) {

        }
    }

    void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = getActivity().getCurrentFocus();

        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    private void createViewPager(ViewPager viewPager) {
        PorfolioTabAdapter adapter = new PorfolioTabAdapter(getFragmentManager());
        adapter.addFrag(new Index(), "Index");
        adapter.addFrag(new SubIndex(), "SubIndex");
        adapter.addFrag(new Company(), "Company");
        viewPager.setAdapter(adapter);

    }

    private void createTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabOne.setText("Index");
        tabs.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabTwo.setText("SubIndex");
        tabs.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custome_tab, null);
        tabThree.setText("Company");
        tabs.getTabAt(2).setCustomView(tabThree);
    }


    @Override
    public void setLossAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList) {
        BaseTableAdapter baseTableAdapter = new DashBoardTableAdapter(getActivity(), headers, tableContentsList);
        tableLoss.setAdapter(baseTableAdapter);

        loadingIndicatorView.setVisibility(View.GONE);
        dashboardTable.setVisibility(View.VISIBLE);
    }

    @Override
    public void setGainAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList) {
        BaseTableAdapter baseTableAdapter = new DashBoardTableAdapter(getActivity(), headers, tableContentsList);
        tableGain.setAdapter(baseTableAdapter);
        dashBoardTablePresenter.getLoss();

        loadingIndicatorView.setVisibility(View.GONE);
        dashboardTable.setVisibility(View.VISIBLE);
    }

    @Override
    public void setTransactionAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList) {
        BaseTableAdapter baseTableAdapter = new DashBoardTableAdapter(getActivity(), headers, tableContentsList);
        tableTransaction.setAdapter(baseTableAdapter);
        dashBoardTablePresenter.getShareTraded();

        loadingIndicatorView.setVisibility(View.GONE);
        dashboardTable.setVisibility(View.VISIBLE);
    }

    @Override
    public void setSharedTradedAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList) {
        BaseTableAdapter baseTableAdapter = new DashBoardTableAdapter(getActivity(), headers, tableContentsList);
        tableSharedTrade.setAdapter(baseTableAdapter);
        dashBoardTablePresenter.getGain();

        loadingIndicatorView.setVisibility(View.GONE);
        dashboardTable.setVisibility(View.VISIBLE);
    }

    @Override
    public void setTurnedOverAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList) {
        BaseTableAdapter baseTableAdapter = new DashBoardTableAdapter(getActivity(), headers, tableContentsList);
        tableTurnOver.setAdapter(baseTableAdapter);
        dashBoardTablePresenter.getGain();

        loadingIndicatorView.setVisibility(View.GONE);
        dashboardTable.setVisibility(View.VISIBLE);
    }

    @Override
    public void dashboardTableData(ArrayList<String> turnOVerHeadersArrayList, ArrayList<ArrayList<String>> turnOverTableContentArrayStrings,
                                   ArrayList<String> shareHeadersArrayList, ArrayList<ArrayList<String>> shareTableContentArrayStrings,
                                   ArrayList<String> gainHeadersArrayList, ArrayList<ArrayList<String>> gainTableContentArrayStrings,
                                   ArrayList<String> lossHeadersArrayList, ArrayList<ArrayList<String>> lossTableContentArrayStrings
            , MarketSummaryModel marketSummaryModel) {
        loadingIndicatorView.setVisibility(View.GONE);
        dashboardTable.setVisibility(View.VISIBLE);

        turnOver.setText(marketSummaryModel.getCurrentIndex());
        totalMarket.setText(marketSummaryModel.getTotalMarketCapitalizationRs());
        totalScips.setText(marketSummaryModel.getTotalScripsTraded());
        tradedShares.setText(marketSummaryModel.getTotalTradedShares());
        transaction.setText(marketSummaryModel.getTotalTransactions());
        flatedMarket.setText(marketSummaryModel.getFloatedMarketCapitalizationRs());
        Log.d("kdsmfdsf", "here");
        BaseTableAdapter lossBaseTableAdapter = new DashBoardTableAdapter(getActivity(), lossHeadersArrayList, lossTableContentArrayStrings);
        tableLoss.setAdapter(lossBaseTableAdapter);

        BaseTableAdapter gainBaseTableAdapter = new DashBoardTableAdapter(getActivity(), gainHeadersArrayList, gainTableContentArrayStrings);
        tableGain.setAdapter(gainBaseTableAdapter);

        BaseTableAdapter shareTableAdapter = new DashBoardTableAdapter(getActivity(), shareHeadersArrayList, shareTableContentArrayStrings);
        tableSharedTrade.setAdapter(shareTableAdapter);

        BaseTableAdapter turnTableAdapter = new DashBoardTableAdapter(getActivity(), turnOVerHeadersArrayList, turnOverTableContentArrayStrings);
        tableTurnOver.setAdapter(turnTableAdapter);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
}