package com.zeronebits.nepalstockwatch.portfolio.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Own on 11/16/2017.
 */

public class BoughtAndSoldSharesParser {

    String result;
    public ArrayList<ArrayList<String>> boughtTableContentArrayStrings;
    public ArrayList<String> boughtHeadersArrayList;
    public ArrayList<ArrayList<String>> soldTableContentArrayStrings;
    public ArrayList<String> soldHeadersArrayList;


    public BoughtAndSoldSharesParser(String result) {
        this.result = result;
        boughtHeadersArrayList = new ArrayList<>();
        boughtTableContentArrayStrings = new ArrayList<>();
        soldHeadersArrayList = new ArrayList<>();
        soldTableContentArrayStrings = new ArrayList<>();

    }

    public void parser() throws JSONException {
        JSONObject jsonObject = new JSONObject(result);
        JSONArray jsonArray = jsonObject.getJSONArray("bought_share");
        JSONArray soldArray = jsonObject.getJSONArray("sold_share");
        boughtHeadersArrayList.add("BUY_DT");
        boughtHeadersArrayList.add("ISSU_TYPE");
        boughtHeadersArrayList.add("PRFL_CODE");
        boughtHeadersArrayList.add("BUY_UNITS");
        boughtHeadersArrayList.add("BUY_RATE");
        for(int i=0; i < jsonArray.length(); i++){
            JSONObject data = jsonArray.getJSONObject(i);
            ArrayList<String> dataStrings = new ArrayList<>();

            Iterator<String> gainKeys= data.keys();
            while (gainKeys.hasNext())
            {
                String keyValue = (String)gainKeys.next();
                String string = data.getString(keyValue);
                dataStrings.add(string);
            }
            boughtTableContentArrayStrings.add(dataStrings);

        }


        soldHeadersArrayList.add("SELL_DT");
        soldHeadersArrayList.add("PRFL_CODE");
        soldHeadersArrayList.add("SELL_UNITS");
        soldHeadersArrayList.add("SELL_RATE");
        for(int i=0; i < soldArray.length(); i++){
            JSONObject data = soldArray.getJSONObject(i);
            ArrayList<String> dataStrings = new ArrayList<>();

            Iterator<String> gainKeys= data.keys();
            while (gainKeys.hasNext())
            {
                String keyValue = (String)gainKeys.next();
                String string = data.getString(keyValue);
                dataStrings.add(string);
            }
            soldTableContentArrayStrings.add(dataStrings);

        }

    }
}
