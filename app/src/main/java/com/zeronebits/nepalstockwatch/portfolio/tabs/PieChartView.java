package com.zeronebits.nepalstockwatch.portfolio.tabs;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;
import com.zeronebits.nepalstockwatch.portfolio.tabs.model.IndexCodeModel;
import com.zeronebits.nepalstockwatch.portfolio.tabs.model.IssueTypeModelPie;

import java.util.ArrayList;

/**
 * Created by Own on 11/9/2017.
 */

public interface PieChartView {
    void indexCodeValue(ArrayList<IndexCodeModel> indexCodeModels,final ArrayList<String> chartData, ArrayList<PieEntry> chartValue);

    void companyTableData(ArrayList<String> headers, ArrayList<ArrayList<String>> contentsWithValue);

    void issueType(ArrayList<IssueTypeModelPie> issueTypeModelPies, ArrayList<String> chartData, ArrayList<PieEntry> chartValue);

    void symbolClicked(String s);
}
