package com.zeronebits.nepalstockwatch.portfolio.tabs.presenter;

/**
 * Created by Own on 11/9/2017.
 */

public interface PieChartPresenter {
    void getPieChartDataForCompany();
    void getPieChartDataForIndex();

    void getPieChartDataForSubIndex();

    void getPieChartForPortfolio();
}
