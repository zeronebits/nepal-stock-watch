package com.zeronebits.nepalstockwatch.portfolio.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Own on 10/25/2017.
 */

public class SellStockModel {

    @SerializedName("CSTM_CODE")
    String cstm_code;
    @SerializedName("SELL_UNITS")
    String sell_units;
    @SerializedName("SELL_RATE")
    String sell_rate;
    @SerializedName("PRFL_CODE")
    String prfl_code;



    public SellStockModel(String cstm_code, String sell_units, String sell_rate, String prfl_code) {
        this.cstm_code = cstm_code;
        this.sell_units = sell_units;
        this.sell_rate = sell_rate;
        this.prfl_code = prfl_code;
    }
}
