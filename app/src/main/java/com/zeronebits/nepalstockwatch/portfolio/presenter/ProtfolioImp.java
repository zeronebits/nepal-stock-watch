package com.zeronebits.nepalstockwatch.portfolio.presenter;

import android.content.Context;
import android.util.Log;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.helper.LoadingHelper;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.portfolio.model.BoughtAndSoldSharesParser;
import com.zeronebits.nepalstockwatch.portfolio.model.BuyStockModel;
import com.zeronebits.nepalstockwatch.portfolio.model.IssueTypeParser;
import com.zeronebits.nepalstockwatch.portfolio.model.SellStockModel;
import com.zeronebits.nepalstockwatch.portfolio.view.PortfolioView;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 10/25/2017.
 */

public class ProtfolioImp implements ProtfolioPresenter {

    PortfolioView portfolioView;
    Context context;

    public ProtfolioImp(PortfolioView portfolioView, Context context) {
        this.portfolioView = portfolioView;
        this.context = context;
    }


    @Override
    public void addBuyStock(BuyStockModel buyStockModel) {
        final LoadingHelper loadingHelper = new LoadingHelper(context);
        loadingHelper.showDialog();
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.addBuyStock(buyStockModel);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("addBuyStock", response.code() + "  " + response.raw().request().url());
                String responseString = bodyToString(response.raw().request().body());
                Log.d("addBuyStock", responseString + " ");
                if(loadingHelper.isShowing()){
                    loadingHelper.disMiss();
                }
                if (response.isSuccessful()) {
                    try {
                        Log.d("sdkfjdsfsdf", response.body().string());
                        portfolioView.addedSucessfully();
//                        watchListView.watchListResponse();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }  else {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                        portfolioView.errorAdding();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void addSellStock(SellStockModel sellStockModel) {
        final LoadingHelper loadingHelper = new LoadingHelper(context);
        loadingHelper.showDialog();
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.addSellStock(sellStockModel);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  ");
                if(loadingHelper.isShowing()){
                    loadingHelper.disMiss();
                }
                if (response.isSuccessful()) {
                    try {
                        Log.d("sdkfjdsfsdf", response.body().string());
                        portfolioView.addedSucessfully();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }/* catch (JSONException e) {
                    e.printStackTrace();
                }
*/

                } else {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

//                    watchListView.emptyWatchList();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getIssueType() {
        final LoadingHelper loadingHelper = new LoadingHelper(context);
        loadingHelper.showDialog();
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getIssuesType();
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  ");
                if(loadingHelper.isShowing()){
                    loadingHelper.disMiss();
                }
                if (response.isSuccessful()) {
                    try {
                        IssueTypeParser issueTypeParser = new IssueTypeParser(response.body().string());
                        issueTypeParser.parser();
                        portfolioView.issueTypeList(issueTypeParser.issueTypeModels);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                    e.printStackTrace();
                }

                } else {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

//                    watchListView.emptyWatchList();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getBoughtOrSoldShares() {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getBoughtAndSoldShares(Constant.USERCODE);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  ");
                if (response.isSuccessful()) {
                    try {
                        BoughtAndSoldSharesParser boughtAndSoldSharesParser = new BoughtAndSoldSharesParser(response.body().string());
                        boughtAndSoldSharesParser.parser();
                        portfolioView.boughtAndSoldtableData(boughtAndSoldSharesParser.boughtHeadersArrayList,
                                boughtAndSoldSharesParser.boughtTableContentArrayStrings, boughtAndSoldSharesParser.soldHeadersArrayList,
                                boughtAndSoldSharesParser.soldTableContentArrayStrings);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

//                    watchListView.emptyWatchList();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }


    private String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}
