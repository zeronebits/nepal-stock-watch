package com.zeronebits.nepalstockwatch.portfolio.view;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inqbarna.tablefixheaders.adapters.BaseTableAdapter;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

/**
 * Created by Own on 11/16/2017.
 */

public class ProtfolioTableAdapter extends BaseTableAdapter {

    Context context;
    ArrayList<ArrayList<String>> contents;
//    ArrayList<Nexus> nexuses;
    //    private final NexusTypes familys[];
    /*private final String headers[] = {
            "Sym",
            "Volume",
            "Value",
            "Cost",
            "Profit",

    };*/

    ArrayList<String> headers;


    /*  private final int[] widths = {
              60,
              60,
              60,
              60,
              60,
              60,
              60,
      };*/
    private final float density;

    public ProtfolioTableAdapter(Context context, ArrayList<String> headers,ArrayList<ArrayList<String>> contents) {
//        nexuses = new ArrayList<>();
        this.context = context;
        this.headers = headers;
        this.contents = contents;
//        nexuses.add(new Nexus("MNBBL", "500", "500", "500", "500"));
//        nexuses.add(new Nexus("MNBBL", "500", "500", "500", "500"));
//        nexuses.add(new Nexus("MNBBL", "500", "500", "500", "500"));
//        nexuses.add(new Nexus("MNBBL", "500", "500", "500", "500"));
//        nexuses.add(new Nexus("MNBBL", "500", "500", "500", "500"));
//        nexuses.add(new Nexus("MNBBL", "500", "500", "500", "500"));
//        nexuses.add(new Nexus("MNBBL", "500", "500", "500", "500"));
//        nexuses.add(new Nexus("MNBBL", "500", "500", "500", "500"));


        density = context.getResources().getDisplayMetrics().density;
    }

    @Override
    public int getRowCount() {
        return contents.size();
    }

    @Override
    public int getColumnCount() {
        return headers.size()-1;
    }

    @Override
    public View getView(int row, int column, View convertView, ViewGroup parent) {
        final View view;
        switch (getItemViewType(row, column)) {
            case 0:
                view = getFirstHeader(row, column, convertView, parent);
                break;
            case 1:
                view = getHeader(row, column, convertView, parent);
                break;
            case 2:
                view = getFirstBody(row, column, convertView, parent);
                break;
            case 3:
                view = getBody(row, column, convertView, parent);
                break;
          /*  case 4:
                view = getFamilyView(row, column, convertView, parent);
                break;*/
            default:
                throw new RuntimeException("wtf?");
        }
        return view;
    }

    private View getFirstHeader(int row, int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_table_header_first, parent, false);
//                convertView = getActivity().getLayoutInflater().inflate(R.layout.item_table_header_first, parent, false);
        }
        ((TextView) convertView.findViewById(android.R.id.text1)).setText(headers.get(0));
        return convertView;
    }

    private View getHeader(int row, int column, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        convertView = inflater.inflate(R.layout.item_table_header, parent, false);
//                convertView =  getActivity().getLayoutInflater().inflate(R.layout.item_table_header, parent, false);
        if(column == headers.size()-1){

        }else {
            Log.d("dsffsdasdasfd", column + " " + headers.get(column));
            ((TextView) convertView.findViewById(android.R.id.text1)).setText(headers.get(column+1));
        }
        return convertView;
    }

    private View getFirstBody(int row, int column, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        convertView = inflater.inflate(R.layout.item_table_first, parent, false);
//                convertView =  getActivity().getLayoutInflater().inflate(R.layout.item_table_first, parent, false);
        if(column == headers.size()-1){

        }else {
            convertView.setBackgroundResource(row % 2 == 0 ? R.drawable.bg_table_color1 : R.drawable.bg_table_color2);
            ((TextView) convertView.findViewById(android.R.id.text1)).setText(getDevice(row).get(column + 1));
        }
        return convertView;
    }

    private View getBody(int row, int column, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        convertView = inflater.inflate(R.layout.item_table, parent, false);
//                convertView =  getActivity().getLayoutInflater().inflate(R.layout.item_table, parent, false);
        if(column == headers.size()-1){

        }else{
            convertView.setBackgroundResource(row % 2 == 0 ? R.drawable.bg_table_color1 : R.drawable.bg_table_color2);
            ((TextView) convertView.findViewById(android.R.id.text1)).setText(getDevice(row).get(column+1));
        }

        return convertView;
    }

    @Override
    public int getWidth(int column) {
        return Math.round(80 * density);
    }

    @Override
    public int getHeight(int row) {
        final int height;
        if (row == -1) {
            height = 35;
        } else if (isFamily(row)) {
            height = 35;
        } else {
            height = 35;
        }
        return Math.round(height * density);
    }

    @Override
    public int getItemViewType(int row, int column) {
        final int itemViewType;
        if (row == -1 && column == -1) {
            itemViewType = 0;
        } else if (row == -1) {
            itemViewType = 1;
        } /*else if (isFamily(row)) {
            itemViewType = 4;
        } */ else if (column == -1) {
            itemViewType = 2;
        } else {
            itemViewType = 3;
        }
        return itemViewType;
    }

    private boolean isFamily(int row) {
        int family = 0;

        return row == 0;
    }


    private ArrayList<String> getDevice(int row) {
        int family = 0;

        return contents.get(row);
    }

    @Override
    public int getViewTypeCount() {
        return 5;
    }
}