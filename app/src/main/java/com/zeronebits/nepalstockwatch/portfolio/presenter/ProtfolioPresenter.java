package com.zeronebits.nepalstockwatch.portfolio.presenter;

import com.zeronebits.nepalstockwatch.portfolio.model.BuyStockModel;
import com.zeronebits.nepalstockwatch.portfolio.model.SellStockModel;

/**
 * Created by Own on 10/25/2017.
 */

public interface ProtfolioPresenter {
    void addBuyStock(BuyStockModel buyStockModel);

    void addSellStock(SellStockModel sellStockModel);

    void getIssueType();

    void getBoughtOrSoldShares();
}
