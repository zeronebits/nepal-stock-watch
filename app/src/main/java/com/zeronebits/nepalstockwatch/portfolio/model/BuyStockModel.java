package com.zeronebits.nepalstockwatch.portfolio.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Own on 10/25/2017.
 */

public class BuyStockModel {


    @SerializedName("CSTM_CODE")
    String cstm_code;
    @SerializedName("BUY_UNITS")
    String buy_units;
    @SerializedName("BUY_RATE")
    String buy_rate;
    @SerializedName("ISSU_TYPE_CODE")
    String issu_type_code;
    @SerializedName("PRFL_CODE")
    String prfl_code;



    public BuyStockModel(String cstm_code, String buy_units, String buy_rate, String issu_type_code, String prfl_code) {
        this.cstm_code = cstm_code;
        this.buy_units = buy_units;
        this.buy_rate = buy_rate;
        this.issu_type_code = issu_type_code;
        this.prfl_code = prfl_code;
    }
}
