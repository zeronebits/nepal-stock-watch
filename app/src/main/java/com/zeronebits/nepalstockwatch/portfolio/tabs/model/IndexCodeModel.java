package com.zeronebits.nepalstockwatch.portfolio.tabs.model;

/**
 * Created by Own on 11/9/2017.
 */

public class IndexCodeModel {

    String indexCode;
    int units;
    int values;

    public String getIndexCode() {
        return indexCode;
    }

    public void setIndexCode(String indexCode) {
        this.indexCode = indexCode;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    public int getValues() {
        return values;
    }

    public void setValues(int values) {
        this.values = values;
    }
}
