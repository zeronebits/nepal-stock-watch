package com.zeronebits.nepalstockwatch.portfolio.view;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.portfolio.model.IssueTypeModel;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

/**
 * Created by Own on 11/8/2017.
 */

public class IssueTypeAdapter extends ArrayAdapter<IssueTypeModel> {

    LayoutInflater flater;

    public IssueTypeAdapter(Activity context, int resouceId, ArrayList<IssueTypeModel> list){

        super(context,resouceId, list);
//        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return rowview(convertView,position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return rowview(convertView,position);
    }

    private View rowview(View convertView , int position){

        IssueTypeModel rowItem = getItem(position);

        viewHolder holder ;
        View rowview = convertView;
        if (rowview==null) {

            holder = new viewHolder();
            flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = flater.inflate(R.layout.listitems_layout, null, false);

            holder.txtTitle = (TextView) rowview.findViewById(R.id.title);
            rowview.setTag(holder);
        }else{
            holder = (viewHolder) rowview.getTag();
        }
        holder.txtTitle.setText(rowItem.getCODE());

        return rowview;
    }

    private class viewHolder{
        TextView txtTitle;
    }
}