package com.zeronebits.nepalstockwatch.dashboard.tabs.presenter;

/**
 * Created by Own on 11/13/2017.
 */

public interface LineChartPresenter {
    void getGraphData(String selectedIndex, int month);

    void getPortfolioData(int month);

    void getTotalAmount(String usercode);

    void getPortfolioList(int i);

    void getGraphDataForCompany(String selectedIndex, int month);
}
