package com.zeronebits.nepalstockwatch.dashboard.view;

import com.github.mikephil.charting.data.Entry;
import com.zeronebits.nepalstockwatch.dashboard.model.MarketSummaryModel;

import java.util.ArrayList;

/**
 * Created by Own on 9/1/2017.
 */

public interface DashBoardView {

    void setLossAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList);

    void setGainAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList);

    void setTransactionAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList);

    void setSharedTradedAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList);

    void setTurnedOverAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList);


    void dashboardTableData(ArrayList<String> turnOVerHeadersArrayList, ArrayList<ArrayList<String>>
            turnOverTableContentArrayStrings, ArrayList<String> shareHeadersArrayList,
          ArrayList<ArrayList<String>> shareTableContentArrayStrings, ArrayList<String> gainHeadersArrayList,
                            ArrayList<ArrayList<String>> gainTableContentArrayStrings, ArrayList<String> lossHeadersArrayList,
                            ArrayList<ArrayList<String>> lossTableContentArrayStrings,MarketSummaryModel marketSummaryModel);

}
