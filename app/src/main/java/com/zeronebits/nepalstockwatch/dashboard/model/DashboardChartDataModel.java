package com.zeronebits.nepalstockwatch.dashboard.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Own on 11/9/2017.
 */

public class DashboardChartDataModel  {


    @SerializedName("DATE")
    @Expose
    private String dATE;
    @SerializedName("INDEX_CODE")
    @Expose
    private String iNDEXCODE;
    @SerializedName("TURNOVER")
    @Expose
    private String tURNOVER;
    @SerializedName("VALUE")
    @Expose
    private Float vALUE;
    @SerializedName("ABS_CHANGE")
    @Expose
    private String aBSCHANGE;
    @SerializedName("PCT_CHANGE")
    @Expose
    private String pCTCHANGE;


    public String getdATE() {
        return dATE;
    }

    public void setdATE(String dATE) {
        this.dATE = dATE;
    }

    public String getiNDEXCODE() {
        return iNDEXCODE;
    }

    public void setiNDEXCODE(String iNDEXCODE) {
        this.iNDEXCODE = iNDEXCODE;
    }

    public String gettURNOVER() {
        return tURNOVER;
    }

    public void settURNOVER(String tURNOVER) {
        this.tURNOVER = tURNOVER;
    }

    public Float getvALUE() {
        return vALUE;
    }

    public void setvALUE(Float vALUE) {
        this.vALUE = vALUE;
    }

    public String getaBSCHANGE() {
        return aBSCHANGE;
    }

    public void setaBSCHANGE(String aBSCHANGE) {
        this.aBSCHANGE = aBSCHANGE;
    }

    public String getpCTCHANGE() {
        return pCTCHANGE;
    }

    public void setpCTCHANGE(String pCTCHANGE) {
        this.pCTCHANGE = pCTCHANGE;
    }
}
