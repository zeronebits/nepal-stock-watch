package com.zeronebits.nepalstockwatch.dashboard.tabs.view;

import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyname.model.CompanyNameModel;
import com.zeronebits.nepalstockwatch.companyname.view.CompanyNameAdapter;
import com.zeronebits.nepalstockwatch.dashboard.model.TotalGainParser;
import com.zeronebits.nepalstockwatch.dashboard.tabs.presenter.LineChartImp;
import com.zeronebits.nepalstockwatch.dashboard.tabs.presenter.LineChartPresenter;
import com.zeronebits.nepalstockwatch.mainapplication.ApplicationMain;
import com.zeronebits.nepalstockwatch.realmdatabase.RealmController;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Own on 11/13/2017.
 */

public class Company extends BaseFragment implements SeekBar.OnSeekBarChangeListener,
        OnChartGestureListener, OnChartValueSelectedListener,LineChartView {

    @BindView(R.id.chart1)
    LineChart lineChart;
    LineChartPresenter lineChartPresenter;
    int month = 1;
    String selectedIndex;
    @BindView(R.id.btn_1_mnth)
    Button OneMonth;
    @BindView(R.id.btn_3_mnth)
    Button threeMonth;
    @BindView(R.id.btn_6_mnth)
    Button sixMonth;
    @BindView(R.id.btn_12_mnth)
    Button TweMonth;
    @BindView(R.id.act_company_name)
    AutoCompleteTextView companySymbol;
    CompanyNameAdapter companyNameAdapter;
    ArrayList<CompanyNameModel> companyNameModels;
    @BindView(R.id.iv_search)
    ImageView search;
    @BindView(R.id.avi)
    LinearLayout loading;
    @Override
    protected int getLayout() {
        return R.layout.line_chart_layout_company;
    }

    @Override
    protected void init(View view) {
        initResume();
    }

    private void initResume() {
        lineChartPresenter = new LineChartImp(this,getActivity());
        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        companyNameModels = realmController.getModelList();
        companyNameAdapter = new CompanyNameAdapter(getActivity(), R.layout.simple_text_view, companyNameModels);
        companySymbol.setAdapter(companyNameAdapter);
        companySymbol.setThreshold(0);
        selectedIndex = companyNameModels.get(0).getCode();
        loading.setVisibility(View.VISIBLE);
        lineChartPresenter.getGraphDataForCompany(selectedIndex, month);
        buttonClicked(0);
        companySymbol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("djfnds12asdasf", companyNameModels.get(position).getCode() + "   "+companyNameModels.get(position).getName() );
                selectedIndex= companyNameModels.get(position).getCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @OnClick(R.id.iv_search)
    public void searchCompany(){
        loading.setVisibility(View.VISIBLE);
        lineChartPresenter.getGraphData(selectedIndex, month);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick(R.id.btn_1_mnth)
    public void oneMnthClick() {
        month =1;
        buttonClicked(0);
        lineChartPresenter.getGraphData(selectedIndex, month);

    }
    @OnClick(R.id.btn_3_mnth)
    public void threeMnthClick() {
        month =3;
        buttonClicked(1);
        lineChartPresenter.getGraphData(selectedIndex, month);

    }

    @OnClick(R.id.btn_6_mnth)
    public void sixMnthClick() {
        month =6;
        buttonClicked(2);
        lineChartPresenter.getGraphData(selectedIndex, month);

    }

    @OnClick(R.id.btn_12_mnth)
    public void yearClick() {
        month =12;
        buttonClicked(3);
        lineChartPresenter.getGraphData(selectedIndex, month);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }


    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "START, x: " + me.getX() + ", y: " + me.getY());
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if (lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            lineChart.highlightValues(null); // or highlightTouch(null) for callback to onNothingSelected(...)
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }


    @Override
    public void setGraphValue(ArrayList<Entry> chartValue, final ArrayList<String> chartData) {
        loading.setVisibility(View.GONE);

        LineDataSet dataset = new LineDataSet(chartValue,selectedIndex);
//        dataset.setCircleRadius(3f);
        dataset.setDrawFilled(true);
        dataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);

      /*  if (Utils.getSDKInt() >= 18) {
            // fill drawable only supported on api level 18 and above
            Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.fade_orange);
            dataset.setFillDrawable(drawable);
        } else {
            dataset.setFillColor(Color.BLACK);
        }*/
//        dataset.setColors(oragneDark);
        LineData data = new LineData(dataset);
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                return chartData.get((int) value);
            }

        });
        lineChart.getAxisLeft().setDrawGridLines(false);
        lineChart.getAxisRight().setDrawGridLines(false);
//        lineChart.getLegend().setEnabled(false);
        lineChart.setDescription(null);
        lineChart.getXAxis().setDrawGridLines(false);
//        lineChart.getXAxis().setDrawLabels(false);
//        lineChart.getAxisLeft().setDrawLabels(false);
        lineChart.getAxisRight().setDrawLabels(false);
        lineChart.getAxisRight().setAxisLineColor(android.R.color.transparent);

        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        lineChart.setData(data);

        lineChart.animateY(1000);
    }

    @Override
    public void nodDataFound() {
        loading.setVisibility(View.GONE);
    }

    @Override
    public void setPortfolioResponse(ArrayList<String> chartData, ArrayList<Entry> chartValue) {

    }

    @Override
    public void totalAmountSucess() {

    }

    @Override
    public void setTableValue(ArrayList<String> headers, ArrayList<ArrayList<String>> contentsWithValue) {

    }

    public void buttonClicked(int position){
        switch (position){
            case 0:
                OneMonth.setSelected(true);
                OneMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.white));
                threeMonth.setSelected(false);
                threeMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                sixMonth.setSelected(false);
                sixMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                TweMonth.setSelected(false);
                TweMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                break;
            case 1:
                OneMonth.setSelected(false);
                OneMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                threeMonth.setSelected(true);
                threeMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.white));
                sixMonth.setSelected(false);
                sixMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                TweMonth.setSelected(false);
                TweMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                break;
            case 2:
                OneMonth.setSelected(false);
                OneMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                threeMonth.setSelected(false);
                threeMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                sixMonth.setSelected(true);
                sixMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.white));
                TweMonth.setSelected(false);
                TweMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                break;
            case 3:
                OneMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                OneMonth.setSelected(false);
                threeMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                threeMonth.setSelected(false);
                sixMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                sixMonth.setSelected(false);
                TweMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.white));
                TweMonth.setSelected(true);
                break;

        }
    }
}
