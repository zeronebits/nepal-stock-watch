package com.zeronebits.nepalstockwatch.dashboard.tabs.view;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.dashboard.model.TotalGainParser;
import com.zeronebits.nepalstockwatch.dashboard.tabs.presenter.LineChartImp;
import com.zeronebits.nepalstockwatch.dashboard.tabs.presenter.LineChartPresenter;
import com.zeronebits.nepalstockwatch.dashboard.view.DashBoardFragment;
import com.zeronebits.nepalstockwatch.portfolio.tabs.PieChartView;
import com.zeronebits.nepalstockwatch.portfolio.tabs.model.IndexCodeModel;
import com.zeronebits.nepalstockwatch.portfolio.tabs.model.IssueTypeModelPie;
import com.zeronebits.nepalstockwatch.portfolio.tabs.presenter.PieChartImp;
import com.zeronebits.nepalstockwatch.portfolio.tabs.presenter.PieChartPresenter;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Own on 11/20/2017.
 */

public class PortfolioByPortfolio extends BaseFragment implements SeekBar.OnSeekBarChangeListener,
        OnChartGestureListener, OnChartValueSelectedListener, LineChartView {

    @BindView(R.id.chart1)
    LineChart lineChart;
    LineChartPresenter lineChartPresenter;

    int month = 1;
    String selectedIndex;
    @BindView(R.id.btn_1_mnth)
    Button OneMonth;
    @BindView(R.id.btn_3_mnth)
    Button threeMonth;
    @BindView(R.id.btn_6_mnth)
    Button sixMonth;
    @BindView(R.id.btn_12_mnth)
    Button TweMonth;
    MainActivity mainActivity;
    @BindView(R.id.tv_total_gain)
    TextView totalGain;
    @BindView(R.id.avi)
    LinearLayout loading;
    DashBoardFragment dashBoardFragment;
    @Override
    protected int getLayout() {
        return R.layout.line_chart_layout_portfolio;
    }

    @Override
    protected void init(View view) {
        loading.setVisibility(View.VISIBLE);
        lineChartPresenter = new LineChartImp(this, getActivity());
        lineChartPresenter.getTotalAmount(Constant.USERCODE);
        buttonClicked(0);
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivity = (MainActivity) getActivity();
        dashBoardFragment = (DashBoardFragment)getParentFragment();
//        setGraphValue(mainActivity.chartValue,mainActivity.chartData);
    }

    @OnClick(R.id.btn_1_mnth)
    public void oneMnthClick() {
        month = 1;
        buttonClicked(0);
        lineChartPresenter.getPortfolioData(month);

    }

    @OnClick(R.id.btn_3_mnth)
    public void threeMnthClick() {
        month = 3;
        buttonClicked(1);
        lineChartPresenter.getPortfolioData(month);

    }

    @OnClick(R.id.btn_6_mnth)
    public void sixMnthClick() {
        month = 6;
        buttonClicked(2);
        lineChartPresenter.getPortfolioData(month);

    }

    @OnClick(R.id.btn_12_mnth)
    public void yearClick() {
        month = 12;
        buttonClicked(3);
        lineChartPresenter.getPortfolioData(month);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }


    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "START, x: " + me.getX() + ", y: " + me.getY());
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if (lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            lineChart.highlightValues(null); // or highlightTouch(null) for callback to onNothingSelected(...)
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }


    @Override
    public void setGraphValue(ArrayList<Entry> chartValue, final ArrayList<String> chartData) {
        LineDataSet dataset = new LineDataSet(chartValue, selectedIndex);
//        dataset.setCircleRadius(3f);
        dataset.setDrawFilled(true);
        dataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);

      /*  if (Utils.getSDKInt() >= 18) {
            // fill drawable only supported on api level 18 and above
            Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.fade_orange);
            dataset.setFillDrawable(drawable);
        } else {
            dataset.setFillColor(Color.BLACK);
        }*/
//        dataset.setColors(oragneDark);
        LineData data = new LineData(dataset);
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                return chartData.get((int) value);
            }

        });
        lineChart.getAxisLeft().setDrawGridLines(false);
        lineChart.getAxisRight().setDrawGridLines(false);
//        lineChart.getLegend().setEnabled(false);
        lineChart.setDescription(null);
        lineChart.getXAxis().setDrawGridLines(false);
//        lineChart.getXAxis().setDrawLabels(false);
//        lineChart.getAxisLeft().setDrawLabels(false);
        lineChart.getAxisRight().setDrawLabels(false);
        lineChart.getAxisRight().setAxisLineColor(android.R.color.transparent);

        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        lineChart.setData(data);

        lineChart.animateY(1000);
    }

    @Override
    public void nodDataFound() {
        loading.setVisibility(View.GONE);
    }

    @Override
    public void setPortfolioResponse(ArrayList<String> chartData, ArrayList<Entry> chartValue) {

        setGraphValue(chartValue,chartData);
        loading.setVisibility(View.GONE);
//        dashBoardFragment.dashBoardTablePresenter.getAllDashboardData();
//        dashBoardFragment.loadingIndicatorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void totalAmountSucess() {
        totalGain.setText("Total Gain: "+TotalGainParser.totalAmount);
        lineChartPresenter.getPortfolioList(1);
    }

    @Override
    public void setTableValue(ArrayList<String> headers, ArrayList<ArrayList<String>> contentsWithValue) {

    }


    public void buttonClicked(int position){
        switch (position){
            case 0:
                OneMonth.setSelected(true);
                OneMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.white));
                threeMonth.setSelected(false);
                threeMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                sixMonth.setSelected(false);
                sixMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                TweMonth.setSelected(false);
                TweMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                break;
            case 1:
                OneMonth.setSelected(false);
                OneMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                threeMonth.setSelected(true);
                threeMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.white));
                sixMonth.setSelected(false);
                sixMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                TweMonth.setSelected(false);
                TweMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                break;
            case 2:
                OneMonth.setSelected(false);
                OneMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                threeMonth.setSelected(false);
                threeMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                sixMonth.setSelected(true);
                sixMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.white));
                TweMonth.setSelected(false);
                TweMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                break;
            case 3:
                OneMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                OneMonth.setSelected(false);
                threeMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                threeMonth.setSelected(false);
                sixMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.blue_background));
                sixMonth.setSelected(false);
                TweMonth.setTextColor(ContextCompat.getColor(getActivity(),R.color.white));
                TweMonth.setSelected(true);
                break;

        }
    }
}