package com.zeronebits.nepalstockwatch.dashboard.model;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Own on 11/9/2017.
 */

public class IndexModelDashboard {

    String result;
    public ArrayList<String> indexList;

    public IndexModelDashboard(String result) {
        this.result = result;
        indexList = new ArrayList<>();
    }

    public void parser() throws JSONException {
        JSONArray jsonArray = new JSONArray(result);
        for(int i =0; i < jsonArray.length(); i++){
            indexList.add(jsonArray.getString(i));
        }
    }
}
