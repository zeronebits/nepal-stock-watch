package com.zeronebits.nepalstockwatch.dashboard.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Own on 11/20/2017.
 */

public class DashboardPortfolioModel {


    @SerializedName("CSTM_CODE")
    @Expose
    private Integer cSTMCODE;
    @SerializedName("DATE")
    @Expose
    private String dATE;
    @SerializedName("VALUE")
    @Expose
    private String vALUE;


    public Integer getcSTMCODE() {
        return cSTMCODE;
    }

    public void setcSTMCODE(Integer cSTMCODE) {
        this.cSTMCODE = cSTMCODE;
    }

    public String getdATE() {
        return dATE;
    }

    public void setdATE(String dATE) {
        this.dATE = dATE;
    }

    public String getvALUE() {
        return vALUE;
    }

    public void setvALUE(String vALUE) {
        this.vALUE = vALUE;
    }
}
