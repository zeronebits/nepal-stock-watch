package com.zeronebits.nepalstockwatch.dashboard.model;

import android.util.Log;

import com.github.mikephil.charting.data.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Own on 11/20/2017.
 */

public class DashboardPortfolioParser {

    public String result;
    public ArrayList<String> chartData;
    public ArrayList<Entry> chartValue;
    public DashboardPortfolioParser(String result) {
        this.result = result;
        chartValue = new ArrayList<>();
        chartData = new ArrayList<>();
    }

    public void parser() throws JSONException {

        JSONArray json = new JSONArray(result);
        for(int i =0; i < json.length(); i++){
            JSONObject jsonObject = json.getJSONObject(i);
            DashboardPortfolioModel portfolioModel = new DashboardPortfolioModel();
            portfolioModel.setcSTMCODE(jsonObject.getInt("CSTM_CODE"));
            portfolioModel.setdATE(jsonObject.getString("DATE"));
            portfolioModel.setvALUE(jsonObject.getString("VALUE"));
            chartData.add(convertToApiFormat(jsonObject.getString("DATE")));
            chartValue.add(new Entry(Float.parseFloat(i + ""), Float.parseFloat(jsonObject.getString("VALUE"))));
        }
    }

    public String convertToApiFormat(String time) {
        String inputPattern = "yyyy-mm-dd";
        String outputPattern = "dd-MMM";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d(str, "checkdate");
        return str;
    }
}
