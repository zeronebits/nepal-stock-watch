package com.zeronebits.nepalstockwatch.forgotpassword.presenter;

/**
 * Created by Own on 1/23/2018.
 */

public interface ForgotPasswordPresenter {
    void resetPassword(String confirmCode, String newPassword, String usercode);
}
