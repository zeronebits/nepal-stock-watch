package com.zeronebits.nepalstockwatch.forgotpassword.presenter;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.forgotpassword.view.ForgotPasswordView;
import com.zeronebits.nepalstockwatch.helper.LoadingHelper;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 1/23/2018.
 */

public class ForgotPasswordImp implements ForgotPasswordPresenter {

    Context context;
    ForgotPasswordView forgotPasswordView;

    public ForgotPasswordImp(Context context, ForgotPasswordView forgotPasswordView) {
        this.context = context;
        this.forgotPasswordView = forgotPasswordView;
    }


    @Override
    public void resetPassword(String confirmCode, String newPassword, String usercode) {
        final LoadingHelper loadingHelper = new LoadingHelper(context);
        loadingHelper.showDialog();
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();
        Log.d("CheckingCountryResponse", "i am here");

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.resetPassowrd(confirmCode, newPassword, usercode);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loadingHelper.disMiss();
                if (response.isSuccessful()) {
                    try {
                        Log.d("sdkfjdsfsdf", response.body().string());
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        forgotPasswordView.successful();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (Exception e) {

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }
}

