package com.zeronebits.nepalstockwatch.forgotpassword.view;

import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;

import com.zeronebits.nepalstockwatch.base.BaseActivity;
import com.zeronebits.nepalstockwatch.forgotpassword.presenter.ForgotPasswordImp;
import com.zeronebits.nepalstockwatch.forgotpassword.presenter.ForgotPasswordPresenter;
import com.zeronebits.nepalstockwatch.login.view.LoginActivity;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.verification.presenter.VerificationImp;
import com.zeronebits.nepalstockwatch.verification.presenter.VerificationPresenter;
import com.zeronebits.nepalstockwatch.verification.view.VerificationView;
import com.zeronebits.nepalwatchstock.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Own on 1/23/2018.
 */

public class ForgotPassword extends BaseActivity implements ForgotPasswordView,VerificationView {

    @BindView(R.id.et_phone_number)
    EditText phoneNumberEt;
    ForgotPasswordPresenter forgotPasswordPresenter;
    VerificationPresenter verificationPresenter;

    @BindView(R.id.et_verification_code)
    EditText verificationCode;
    @BindView(R.id.et_new_confirm_password)
    EditText newPasswordConfirmEt;
    @BindView(R.id.et_new_password)
    EditText newPasswordEt;
    @BindView(R.id.btn_submit_forgot_password)
    Button btnForgotPassword;

    @Override
    protected int getLayout() {
        return R.layout.forgot_password;
    }

    @Override
    protected void init() {
        forgotPasswordPresenter = new ForgotPasswordImp(this,this);
        verificationPresenter = new VerificationImp(this, this);
    }

    @OnClick(R.id.btn_submit)
    public void forgotPassword(){
        String phoneNumber = phoneNumberEt.getText().toString();
        if(phoneNumber.isEmpty()){
            phoneNumberEt.setError("Required");
        }else{
            Constant.USERCODE = phoneNumber.trim();
            verificationPresenter.resendVerificationCode();
        }
    }


    @OnClick(R.id.btn_submit_forgot_password)
    public void resetForgotPassword(){
        String confirmCode = verificationCode.getText().toString();
        String newPassword = newPasswordEt.getText().toString();
        String newPasswordConfirm = newPasswordConfirmEt.getText().toString();
        if(confirmCode.isEmpty()){
            verificationCode.setError("Required");
        }else if(newPassword.isEmpty()){
            newPasswordEt.setError("Required");
        }else if(newPasswordConfirm.isEmpty()){
            newPasswordConfirmEt.setError("Required");
        }else{
            forgotPasswordPresenter.resetPassword(confirmCode,newPassword,Constant.USERCODE);
        }
    }


    @Override
    public void codeResendSucess() {

    }

    @Override
    public void successful() {
        Intent i = new Intent(this, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);

    }
}
