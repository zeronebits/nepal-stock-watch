package com.zeronebits.nepalstockwatch.realmdatabase;


import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import com.zeronebits.nepalstockwatch.alerts.model.MessagingModel;
import com.zeronebits.nepalstockwatch.companyname.model.CompanyNameModel;
import com.zeronebits.nepalstockwatch.companyname.model.IndexModel;
import com.zeronebits.nepalstockwatch.companyname.model.SubIndexModel;
import com.zeronebits.nepalstockwatch.watchlist.model.WatchListModelDatabase;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;


public class RealmController {

    public static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {
        return realm;
    }

    //Refresh the realm istance

    public void refresh() {
        realm.refresh();
    }

    public void clearAllCompanyNames() {
        realm.beginTransaction();
        realm.delete(CompanyNameModel.class);
        realm.commitTransaction();
    }

    public void addCompanyName(CompanyNameModel companyNameModel) {
        realm.beginTransaction();
        realm.copyToRealm(companyNameModel);
        realm.commitTransaction();
    }

    public void addMessage(MessagingModel messagingModel) {
        realm.beginTransaction();
        realm.copyToRealm(messagingModel);
        realm.commitTransaction();
    }

    public ArrayList<CompanyNameModel> getModelList() {
        ArrayList<CompanyNameModel> list = new ArrayList<>();
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            RealmResults<CompanyNameModel> results = realm
                    .where(CompanyNameModel.class)
                    .findAll();
            list.addAll(realm.copyFromRealm(results));
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        return list;
    }

   public ArrayList<MessagingModel> getMessageList() {
        ArrayList<MessagingModel> list = new ArrayList<>();
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            RealmResults<MessagingModel> results = realm
                    .where(MessagingModel.class)
                    .findAll();
            list.addAll(realm.copyFromRealm(results));
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        return list;
    }


    /*index*/

    public void clearAllIndex() {
        realm.beginTransaction();
        realm.delete(IndexModel.class);
        realm.commitTransaction();
    }

    public void addIndex(IndexModel indexModel) {
        realm.beginTransaction();
        realm.copyToRealm(indexModel);
        realm.commitTransaction();
    }

    public ArrayList<IndexModel> getIndex() {
        ArrayList<IndexModel> list = new ArrayList<>();
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            RealmResults<IndexModel> results = realm
                    .where(IndexModel.class)
                    .findAll();
            list.addAll(realm.copyFromRealm(results));
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        return list;
    }

    /*subindex*/

    public void clearAllSubIndex() {
        realm.beginTransaction();
        realm.delete(CompanyNameModel.class);
        realm.commitTransaction();
    }

    public void addSubIndex(SubIndexModel subIndexModel) {
        realm.beginTransaction();
        realm.copyToRealm(subIndexModel);
        realm.commitTransaction();
    }

    public ArrayList<SubIndexModel> getSubIndex() {
        ArrayList<SubIndexModel> list = new ArrayList<>();
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            RealmResults<SubIndexModel> results = realm
                    .where(SubIndexModel.class)
                    .findAll();
            list.addAll(realm.copyFromRealm(results));
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        return list;
    }


    /*watch list*/

    public void clearAllWatchList() {
        realm.beginTransaction();
        realm.delete(WatchListModelDatabase.class);
        realm.commitTransaction();
    }

    public void addWatchList(WatchListModelDatabase watchListUserModel) {
        realm.beginTransaction();
        realm.copyToRealm(watchListUserModel);
        realm.commitTransaction();
    }

    public ArrayList<WatchListModelDatabase> getWatchList() {
        ArrayList<WatchListModelDatabase> list = new ArrayList<>();
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            RealmResults<WatchListModelDatabase> results = realm
                    .where(WatchListModelDatabase.class)
                    .findAll();
            list.addAll(realm.copyFromRealm(results));
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
        return list;
    }
}
