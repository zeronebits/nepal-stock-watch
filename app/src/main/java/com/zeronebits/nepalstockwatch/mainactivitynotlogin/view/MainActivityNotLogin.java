package com.zeronebits.nepalstockwatch.mainactivitynotlogin.view;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.data.Entry;
import com.zeronebits.nepalstockwatch.base.BaseActivity;
import com.zeronebits.nepalstockwatch.dashboardnotlogin.view.DashBoardNotLogin;
import com.zeronebits.nepalstockwatch.login.view.LoginActivity;
import com.zeronebits.nepalstockwatch.register.view.RegisterActivity;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Own on 8/29/2017.
 */

public class MainActivityNotLogin extends BaseActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tt_icon_login)
    TextView toolBarLogin;
    @BindView(R.id.tv_toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.ll_login_logout)
    LinearLayout loginLogout;

    public ArrayList<String> chartData;
    public ArrayList<Entry> chartValue;
    @Override
    protected int getLayout() {
        return R.layout.activity_main_not_login;
    }

    @Override
    protected void init() {
        setToolbar();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fl_activity_main_not_login, new DashBoardNotLogin(),"tag").
                addToBackStack("tag").commit();
    }


    void setToolbar() {
        setSupportActionBar(toolbar);
        toolBarLogin.setText("\uf213");
        loginLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent b = new Intent(MainActivityNotLogin.this, LoginActivity.class);
                startActivity(b);
                finish();
            }
        });
    }

    public void hideHomeButton(){
       /* getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);*/
        toolbar.setNavigationIcon(null);

    }
    public void setToolBarTitle(String title) {
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarTitle.setText(title);

        toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.mipmap.back_arrow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(getSupportFragmentManager().findFragmentByTag("tag")  == null){
            toolbar.setNavigationIcon(null);
            toolbarTitle.setVisibility(View.GONE);
            finish();
        }
    }
}
