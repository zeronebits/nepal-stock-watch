package com.zeronebits.nepalstockwatch.livetrading.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.inqbarna.tablefixheaders.adapters.BaseTableAdapter;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

/**
 * Created by Own on 9/2/2017.
 */

public class LiveTradingAdapter extends BaseTableAdapter implements Filterable {

    Context context;
    ResturantFilter resturantFilter;
    int editTextPosition;
    ResturantFilter filter;
    boolean flagToShowPerOrVal = true;
    LiveTradingView liveTradingView;
    public int getEditTextPosition() {
        return editTextPosition;
    }

    public void setEditTextPosition(int editTextPosition) {
        this.editTextPosition = editTextPosition;
    }

    ArrayList<String> headers;
    ArrayList<String> diffPercentage;
    ArrayList<ArrayList<String>> contents;
    ArrayList<String> diffValue;
    private final float density;

    public LiveTradingAdapter(Context context, ArrayList<String> headers, ArrayList<ArrayList<String>>
            contents, ArrayList<String> diffPercentage, ArrayList<String> diffValue,LiveTradingView liveTradingView) {
//        nexuses = new ArrayList<>();
        this.context = context;
        this.headers = headers;
        this.contents = contents;
        this.diffPercentage = diffPercentage;
        this.diffValue = diffValue;
        density = context.getResources().getDisplayMetrics().density;
        this.liveTradingView = liveTradingView;
    }


    @Override
    public int getRowCount() {
        return contents.size();
    }

    @Override
    public int getColumnCount() {
        return headers.size() - 2;
    }

    @Override
    public View getView(int row, int column, View convertView, ViewGroup parent) {
        final View view;
        switch (getItemViewType(row, column)) {
            case 0:
                view = getFirstHeader(row, column, convertView, parent);
                break;
            case 1:
                view = getHeader(row, column, convertView, parent);
                break;
            case 2:
                view = getFirstBody(row, column, convertView, parent);
                break;
            case 3:
                view = getBody(row, column, convertView, parent);
                break;
            default:
                throw new RuntimeException("wtf?");
        }
        return view;
    }

    private View getFirstHeader(int row, int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_table_header_first, parent, false);
            TextView contentName = (TextView) convertView.findViewById(android.R.id.text1);


            if (column == headers.size() - 1) {
                if (flagToShowPerOrVal) {
                    contentName.setText(headers.get(0));
                }
            } else {
                contentName.setText(headers.get(0));
            }
        }
        return convertView;
    }

    private View getHeader(int row, int column, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        convertView = inflater.inflate(R.layout.item_table_header, parent, false);
        TextView contentName = (TextView) convertView.findViewById(android.R.id.text1);
        if (column == headers.size() - 1) {

        } else {
            if (column == headers.size() - 3) {
                if (flagToShowPerOrVal) {
                    contentName.setText("Diff(Value)");
                } else {
                    contentName.setText("Diff(%)");
                }
            } else {
                contentName.setText(headers.get(column + 1));
            }

        }
//        }


        return convertView;
    }

    private View getFirstBody(final int row, final int column, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        convertView = inflater.inflate(R.layout.item_table_first, parent, false);
        TextView contentName = (TextView) convertView.findViewById(android.R.id.text1);
        if (column == headers.size() - 1) {

        } else {
            convertView.setBackgroundResource(row % 2 == 0 ? R.drawable.bg_table_color1 : R.drawable.bg_table_color2);
            contentName.setText(getDevice(row).get(column + 1));
            SpannableString content = new SpannableString(getDevice(row).get(column + 1));
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            contentName.setText(content);
            contentName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    liveTradingView.symbolClicked(getDevice(row).get(column + 1));
                }
            });
        }
        return convertView;
    }

    private View getBody(final int row, final int column, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        convertView = inflater.inflate(R.layout.item_table, parent, false);
        TextView contentName = (TextView) convertView.findViewById(android.R.id.text1);
        if (column == headers.size() - 1) {
        } else {

            if (column == headers.size() - 3) {
                Log.d("dfsdfsdf", diffPercentage.size() + " ");
                if (flagToShowPerOrVal) {
                    convertView.setBackgroundResource(row % 2 == 0 ? R.drawable.bg_table_color1 : R.drawable.bg_table_color2);
                    contentName.setText(diffPercentage.get(row));
                    if (diffPercentage.get(row).contains("-")) {
                        contentName.setTextColor(ContextCompat.getColor(context, R.color.text_color_red));
                    } else {
                        contentName.setTextColor(ContextCompat.getColor(context, R.color.text_color_green));
                    }
                    contentName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            flagToShowPerOrVal = false;
                            notifyDataSetChanged();
                        }
                    });
                } else {
                    convertView.setBackgroundResource(row % 2 == 0 ? R.drawable.bg_table_color1 : R.drawable.bg_table_color2);
                    contentName.setText(diffValue.get(row));
                    if (diffValue.get(row).contains("-")) {
                        contentName.setTextColor(ContextCompat.getColor(context, R.color.text_color_red));
                    } else {
                        contentName.setTextColor(ContextCompat.getColor(context, R.color.text_color_green));
                    }
                    contentName.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            flagToShowPerOrVal = true;
                            notifyDataSetChanged();
                        }
                    });
                }
            } else {
                convertView.setBackgroundResource(row % 2 == 0 ? R.drawable.bg_table_color1 : R.drawable.bg_table_color2);
                contentName.setText(getDevice(row).get(column + 1));
            }

        }
        return convertView;
    }

    @Override
    public int getWidth(int column) {
        return Math.round(55 * density);
    }

    @Override
    public int getHeight(int row) {
        final int height;
        if (row == -1) {
            height = 35;
        } else if (isFamily(row)) {
            height = 35;
        } else {
            height = 35;
        }
        return Math.round(height * density);
    }

    @Override
    public int getItemViewType(int row, int column) {
        final int itemViewType;
        if (row == -1 && column == -1) {
            itemViewType = 0;
        } else if (row == -1) {
            itemViewType = 1;
        } /*else if (isFamily(row)) {
            itemViewType = 4;
        } */ else if (column == -1) {
            itemViewType = 2;
        } else {
            itemViewType = 3;
        }
        return itemViewType;
    }

    private boolean isFamily(int row) {
        int family = 0;

        return row == 0;
    }


    private ArrayList<String> getDevice(int row) {
        int family = 0;

        return contents.get(row);
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public Filter getFilter() {
        if (resturantFilter == null)
            resturantFilter = new ResturantFilter(this, contents, diffValue, diffPercentage);
        return resturantFilter;
    }


    private static class ResturantFilter extends Filter {

        private final LiveTradingAdapter adapter;
        private final ArrayList<ArrayList<String>> originalList;
        private final ArrayList<ArrayList<String>> filteredList;
        public final ArrayList<String> filterDiffValue;
        private final ArrayList<String> orginalDiffValue;
        public final ArrayList<String> filterDiffPer;
        private final ArrayList<String> orginalDiffPerce;


        private ResturantFilter(LiveTradingAdapter adapter, ArrayList<ArrayList<String>> originalList,
                                ArrayList<String> OrginalDiffValue, ArrayList<String> OrginaldiffPerce) {
            super();
            this.adapter = adapter;
            this.originalList = new ArrayList<>(originalList);
            this.filteredList = new ArrayList<>();
            this.orginalDiffPerce = new ArrayList<>(OrginaldiffPerce);
            this.orginalDiffValue = new ArrayList<>(OrginalDiffValue);
            this.filterDiffValue = new ArrayList<>();
            this.filterDiffPer = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            filterDiffPer.clear();
            filterDiffValue.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filteredList.addAll(originalList);
                filterDiffPer.addAll(orginalDiffPerce);
                filterDiffValue.addAll(orginalDiffValue);
            } else {
                for (final ArrayList<String> resturant : originalList) {
                    final String filterPattern = constraint.toString().toLowerCase().trim();

                    if (resturant.get(adapter.getEditTextPosition()).toLowerCase().trim().contains(filterPattern)) {
                        filteredList.add(resturant);
                        Log.d("jsdhfsdkjfdsf", originalList.indexOf(resturant) + "  ");
                        Log.d("jsdhfsdkjfdsf", orginalDiffPerce.get(originalList.indexOf(resturant)) + "  ");
                        filterDiffPer.add(orginalDiffPerce.get(originalList.indexOf(resturant)));
                        filterDiffValue.add(orginalDiffValue.get(originalList.indexOf(resturant)));
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.contents.clear();
            adapter.diffValue.clear();
            adapter.diffPercentage.clear();
            adapter.contents.addAll((ArrayList<ArrayList<String>>) results.values);
            Log.d("sdkfndsf",filterDiffValue.size() + "   "+ filterDiffPer.size());
            adapter.diffValue.addAll(filterDiffValue);
            adapter.diffPercentage.addAll(filterDiffPer);
            adapter.notifyDataSetChanged();
        }
    }
}

