package com.zeronebits.nepalstockwatch.livetrading.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.inqbarna.tablefixheaders.TableFixHeaders;
import com.wang.avi.AVLoadingIndicatorView;
import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyname.model.CompanyNameModel;
import com.zeronebits.nepalstockwatch.companyname.view.CompanyNameAdapter;
import com.zeronebits.nepalstockwatch.companyprofile.view.CompanyProfile;
import com.zeronebits.nepalstockwatch.livetrading.presenter.LiveTradingImplementor;
import com.zeronebits.nepalstockwatch.livetrading.presenter.LiveTradingPresenter;
import com.zeronebits.nepalstockwatch.mainactivitynotlogin.view.MainActivityNotLogin;
import com.zeronebits.nepalstockwatch.mainapplication.ApplicationMain;
import com.zeronebits.nepalstockwatch.realmdatabase.RealmController;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.watchlist.model.WatchListModelDatabase;
import com.zeronebits.nepalwatchstock.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import butterknife.BindView;

/**
 * Created by Own on 9/2/2017.
 */

public class LiveTradingFragment extends BaseFragment implements LiveTradingView {
    @BindView(R.id.table)
    TableFixHeaders tableFixHeaders;
    @BindView(R.id.avi)
    AVLoadingIndicatorView loadingIndicatorView;
    LiveTradingAdapter baseTableAdapter;
    @BindView(R.id.sw_live_trading_troggle)
    Switch watchListOrAll;
    ArrayList<ArrayList<String>> tableContentsList;
    ArrayList<String> headers;
    ArrayList<String> diffPer;
    CompanyNameAdapter companyNameAdapter;
    @BindView(R.id.act_company_name)
    AutoCompleteTextView companySymbol;
    ArrayList<String> value;
    MainActivity mainActivity;
    MainActivityNotLogin mainActivityNotLogin;

    @BindView(R.id.ll_livetrading_error)
    LinearLayout liveTradingError;

    @Override
    protected int getLayout() {
        return R.layout.live_trading;
    }

    @Override
    protected void init(View view) {
        LiveTradingPresenter liveTradingPresenter = new LiveTradingImplementor(this,getActivity());
        liveTradingPresenter.getLiveTradingData();
        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        ArrayList<CompanyNameModel> companyNameModels = realmController.getModelList();
        Log.d("fndsjfsdf", companyNameModels.size() + "  ");
        companySymbol.setThreshold(0);

        companyNameAdapter = new CompanyNameAdapter(getActivity(), R.layout.simple_text_view, companyNameModels);
        companySymbol.setAdapter(companyNameAdapter);
        watchListOrAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    baseTableAdapter = new LiveTradingAdapter(getActivity(), headers, tableContentsList, diffPer, value,LiveTradingFragment.this);
                    tableFixHeaders.setAdapter(baseTableAdapter);
                } else {
                    baseTableAdapter = new LiveTradingAdapter(getActivity(), headers, getWatchListContents(), diffPer, value,LiveTradingFragment.this);
                    tableFixHeaders.setAdapter(baseTableAdapter);

                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mainActivity = (MainActivity) getActivity();
            mainActivity.setToolBarTitle("Live Trading");
        }catch (Exception e){
            mainActivityNotLogin = (MainActivityNotLogin) getActivity();
            mainActivityNotLogin.setToolBarTitle("Live Trading");
        }
    }

    private ArrayList<ArrayList<String>> getWatchListContents() {
        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        ArrayList<WatchListModelDatabase> watchListUserModels = realmController.getWatchList();
        ArrayList<ArrayList<String>> watchList = new ArrayList<>();
        for(WatchListModelDatabase watchListModelDatabase: watchListUserModels){
            Log.d("dfdsfsdfsdf",watchListModelDatabase.getCompanySymbol());
            for (final ArrayList<String> contents : tableContentsList) {
                if (contents.contains(watchListModelDatabase.getCompanySymbol())) {
                    watchList.add(contents);
                }else{
                    Log.d("dfdsfsdfsdf","hjrtr");
                }
            }
        }


        return watchList;
    }


    @Override
    public void setAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList,
                           ArrayList<String> diffPer, ArrayList<String> value) {
        try {
            mainActivity.setToolBarTitle("Live Trading (" + getDate() + ")");
        }catch (Exception e){
            mainActivityNotLogin.setToolBarTitle("Live Trading (" + getDate() + ")");
        }
        loadingIndicatorView.setVisibility(View.GONE);
        tableFixHeaders.setVisibility(View.VISIBLE);
        this.tableContentsList = tableContentsList;
        this.diffPer =diffPer;
        this.value = value;
        this.headers = headers;
        baseTableAdapter = new LiveTradingAdapter(getActivity(), headers, tableContentsList, diffPer, value,this);
        tableFixHeaders.setAdapter(baseTableAdapter);

        companySymbol.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
//                    buyerEt.setText("");
//                companySymbol.setText("");
                if (baseTableAdapter != null) {
                    baseTableAdapter.setEditTextPosition(0);
                    baseTableAdapter.getFilter().filter(cs);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    @Override
    public void symbolClicked(String s) {
        Fragment fragment = new CompanyProfile();
        Bundle b = new Bundle();
        b.putString(Constant.COMPANYCODE,s);
        fragment.setArguments(b);
        if(Constant.ACESS_TOKEN.equalsIgnoreCase("")) {
            getFragmentManager().beginTransaction().replace(R.id.fl_activity_main_not_login, fragment).addToBackStack("tag").commit();
        }else{
            getFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame,fragment).addToBackStack("tag").commit();
        }
    }

    @Override
    public void setError() {
        loadingIndicatorView.setVisibility(View.GONE);
        tableFixHeaders.setVisibility(View.GONE);
        liveTradingError.setVisibility(View.VISIBLE);
    }


    public String getDate(){
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        return timeStamp;
    }
}
