package com.zeronebits.nepalstockwatch.livetrading.model;

import android.widget.ArrayAdapter;

import java.util.ArrayList;

/**
 * Created by Own on 9/2/2017.
 */

public class LiveTradingModel {
    ArrayList<String> headers;
    ArrayAdapter<ArrayList<String>> contents;

    public ArrayList<String> getHeaders() {
        return headers;
    }

    public void setHeaders(ArrayList<String> headers) {
        this.headers = headers;
    }

    public ArrayAdapter<ArrayList<String>> getContents() {
        return contents;
    }

    public void setContents(ArrayAdapter<ArrayList<String>> contents) {
        this.contents = contents;
    }
}
