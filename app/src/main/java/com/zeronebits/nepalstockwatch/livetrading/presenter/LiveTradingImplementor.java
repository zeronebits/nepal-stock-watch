package com.zeronebits.nepalstockwatch.livetrading.presenter;

import android.content.Context;
import android.util.Log;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.dashboard.tabs.view.Company;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.livetrading.model.LiveTradingParser;
import com.zeronebits.nepalstockwatch.livetrading.view.LiveTradingView;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 9/2/2017.
 */

public class LiveTradingImplementor implements LiveTradingPresenter {

    ArrayList<ArrayList<String>> tableContentsList;
    ArrayList<String> headers;
    LiveTradingView liveTradingView;
    ArrayList<String> diffPercentage;
    ArrayList<String> diffValue;
    Context context;

    public LiveTradingImplementor(LiveTradingView liveTradingView, Context context) {
        this.liveTradingView = liveTradingView;
        this.context = context;
    }

    @Override
    public void getLiveTradingData() {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getLiveTrading();
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("totalGain", response.raw().request().url() + "  ");
                if(response.isSuccessful()) {
                    try {
                        LiveTradingParser liveTradingParser = new LiveTradingParser(response.body().string());
                        liveTradingParser.parse();
                        tableContentsList = liveTradingParser.getContentsWithValue();
                        headers = liveTradingParser.getHeaders();
                        diffPercentage = liveTradingParser.getDiffPercentage();
                        diffValue = liveTradingParser.getDiffValue();
                        liveTradingView.setAdapter(headers, tableContentsList, diffPercentage, diffValue);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    liveTradingView.setError();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }
}
