package com.zeronebits.nepalstockwatch.livetrading.view;

import java.util.ArrayList;

/**
 * Created by Own on 9/2/2017.
 */

public interface LiveTradingView {
    void setAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> tableContentsList,ArrayList<String> diffPer,ArrayList<String> value);

    void symbolClicked(String s);

    void setError();
}
