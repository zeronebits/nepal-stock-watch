package com.zeronebits.nepalstockwatch.alerts.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Own on 12/7/2017.
 */

public class UserAlertsParser {
    String result;
    public ArrayList<UserAlertsModel> userAlertsModels;

    public UserAlertsParser(String result) {
        this.result = result;
        userAlertsModels = new ArrayList<>();
    }

    public void parser() throws JSONException {
        JSONArray json = new JSONArray(result);
                for(int i =0; i < json.length(); i++){
                    UserAlertsModel userAlertsModel = new UserAlertsModel();
                    JSONObject jsonObject = json.getJSONObject(i);
                    userAlertsModel.setCode(jsonObject.getString("ALRT_CODE"));
                    userAlertsModel.setDesc(jsonObject.getString("PRFL_CODE"));
                    userAlertsModels.add(userAlertsModel);
                }
    }
}
