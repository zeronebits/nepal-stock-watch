package com.zeronebits.nepalstockwatch.alerts.view;

import com.zeronebits.nepalstockwatch.alerts.model.AlertsModel;
import com.zeronebits.nepalstockwatch.alerts.model.CustomModelAlerts;
import com.zeronebits.nepalstockwatch.alerts.model.UserAlertsModel;

import java.util.ArrayList;

/**
 * Created by Own on 12/3/2017.
 */

public interface AlertsView {

    void sucessfull();

    void userAlertsList(ArrayList<UserAlertsModel> userAlertsModels);

    void deleteItem(String code, String desc);

    void alertsLists(ArrayList<AlertsModel> alertsModels, ArrayList<CustomModelAlerts> customModelAlertses);
}
