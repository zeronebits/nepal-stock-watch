package com.zeronebits.nepalstockwatch.alerts.presenter;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.zeronebits.nepalstockwatch.alerts.model.AlertsParser;
import com.zeronebits.nepalstockwatch.alerts.model.UserAlertsParser;
import com.zeronebits.nepalstockwatch.alerts.view.AlertsView;
import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.helper.LoadingHelper;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;
import com.zeronebits.nepalstockwatch.watchlist.model.WatchListParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 12/3/2017.
 */

public class AlertsImp implements AlertsPresenter{

    AlertsView alertsView;
    Context context;

    public AlertsImp(AlertsView alertsView, Context context) {
        this.alertsView = alertsView;
        this.context = context;
    }

    @Override
    public void getAlertsList() {
        final LoadingHelper loadingHelper = new LoadingHelper(context);
        loadingHelper.showDialog();    SetupRetrofit setupRetrofit = new SetupRetrofit();
        Log.d("dfjdfsdf",Constant.ACESS_TOKEN);
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Log.d("sajdsadsad",Constant.USERCODE);
        Call<ResponseBody> getCountry = loginRegInterface.getAlertsList();
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountrydase", response.code() + "  "+response.raw().request().url());
                loadingHelper.disMiss();
                if (response.isSuccessful()) {
                    try {
                        AlertsParser alertsParser = new AlertsParser(response.body().string());
                        alertsParser.parser();
                        alertsView.alertsLists(alertsParser.alertsModels,alertsParser.customModelAlertses);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if(response.code() == 422) {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getUserAlertsList(String userCode) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Log.d("dfjdfsdf",Constant.ACESS_TOKEN);
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Log.d("sajdsadsad",Constant.USERCODE);
        Call<ResponseBody> getCountry = loginRegInterface.getUserAlertsList(userCode);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountrydase", response.code() + "  "+response.raw().request().url());

                if (response.isSuccessful()) {
                    try {
                        UserAlertsParser alertsParser = new UserAlertsParser(response.body().string());
                        alertsParser.parser();
                        alertsView.userAlertsList(alertsParser.userAlertsModels);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if(response.code() == 422) {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void addAlerts(int issueAlerts, String companyName, String usercode) {
        final LoadingHelper loadingHelper = new LoadingHelper(context);
        loadingHelper.showDialog();
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Log.d("dfjdfsdf",Constant.ACESS_TOKEN);
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Log.d("sajdsadsad",Constant.USERCODE);
        Call<ResponseBody> getCountry = loginRegInterface.addAlertsUser(issueAlerts,companyName,usercode);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountrydase", response.code() + "  "+response.raw().request().url());
                loadingHelper.disMiss();
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        alertsView.sucessfull();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if(response.code() == 422) {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }else{
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loadingHelper.disMiss();
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void deleteAlerts(String code, String desc) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Log.d("dfjdfsdf",Constant.ACESS_TOKEN);
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Log.d("sajdsadsad",Constant.USERCODE);
        Call<ResponseBody> getCountry = loginRegInterface.deleteAlerts(code,desc,Constant.USERCODE);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountrydase", response.code() + "  "+response.raw().request().url());

                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        alertsView.sucessfull();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void addAlertsCategory(String categoryName, String companyName, String usercode) {
        final LoadingHelper loadingHelper = new LoadingHelper(context);
        loadingHelper.showDialog();
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Log.d("dfjdfsdf",Constant.ACESS_TOKEN);
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Log.d("sajdsadsad",Constant.USERCODE);
        Call<ResponseBody> getCountry = loginRegInterface.addAlertsUserCategory(categoryName,companyName,usercode);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountrydase", response.code() + "  "+response.raw().request().url());
                loadingHelper.disMiss();
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        alertsView.sucessfull();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if(response.code() == 422) {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }else{
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loadingHelper.disMiss();
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }
}
