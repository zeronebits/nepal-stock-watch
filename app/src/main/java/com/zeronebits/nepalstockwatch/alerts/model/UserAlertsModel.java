package com.zeronebits.nepalstockwatch.alerts.model;

/**
 * Created by Own on 12/7/2017.
 */

public class UserAlertsModel {

    String code;
    String desc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
