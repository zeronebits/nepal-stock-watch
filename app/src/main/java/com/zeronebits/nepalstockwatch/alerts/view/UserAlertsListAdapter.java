package com.zeronebits.nepalstockwatch.alerts.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.alerts.model.AlertsModel;
import com.zeronebits.nepalstockwatch.alerts.model.UserAlertsModel;
import com.zeronebits.nepalstockwatch.floorsheet.view.FloorSheetAdapterRecycler;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Own on 12/7/2017.
 */

public class UserAlertsListAdapter extends RecyclerView.Adapter<UserAlertsListAdapter.ViewHolder> {

    ArrayList<UserAlertsModel> userAlertsModels;
    Context context;
    ArrayList<AlertsModel> alertsModels;
    AlertsView alertsView;

    public UserAlertsListAdapter(ArrayList<UserAlertsModel> userAlertsModels,Context context,
                                 ArrayList<AlertsModel> alertsModels,AlertsView alertsView) {
        this.userAlertsModels = userAlertsModels;
        this.context = context;
        this.alertsModels =alertsModels;
        this.alertsView = alertsView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_list_alerts_single,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final UserAlertsModel userAlertsModel = userAlertsModels.get(position);
        holder.alertsName.setText(getAlertsName(userAlertsModel.getCode()) + " of "+ userAlertsModel.getDesc());
        holder.deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertsView.deleteItem(userAlertsModel.getCode(),userAlertsModel.getDesc());
            }
        });
    }

    private String getAlertsName(String code) {
        String value = "";
        for(AlertsModel alertsModel : alertsModels){
            if(String.valueOf(alertsModel.getcODE()).equalsIgnoreCase(code)){
                value = alertsModel.getdESCR();
                break;
            }
        }
        return value;
    }

    @Override
    public int getItemCount() {
        return userAlertsModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_alerts_name)
        TextView alertsName;
        @BindView(R.id.iv_cart_delete)
        LinearLayout deleteItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
