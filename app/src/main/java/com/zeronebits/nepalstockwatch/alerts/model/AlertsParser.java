package com.zeronebits.nepalstockwatch.alerts.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Own on 12/4/2017.
 */

public class AlertsParser {

    String result;
    public ArrayList<AlertsModel> alertsModels;
    public ArrayList<CustomModelAlerts> customModelAlertses;
    String currentCategory = "";

    public AlertsParser(String result) {
        this.result = result;
        alertsModels= new ArrayList<>();
        customModelAlertses = new ArrayList<>();
    }

    public void parser() throws JSONException {
        Log.d("fdskkfjsdf",result);
        JSONArray json = new JSONArray(result);
        for(int i=0; i<json.length();i++){
            JSONObject data = json.getJSONObject(i);
            AlertsModel alertsModel = new AlertsModel();
            CustomModelAlerts customModelAlerts = new CustomModelAlerts();
            CustomModelAlerts customModelAlerts1 = new CustomModelAlerts();
            alertsModel.setcODE(data.getInt("CODE"));
            alertsModel.setdESCR(data.getString("DESCR"));
            alertsModel.setCategory(data.getString("CATEGORY"));
            if(currentCategory.equalsIgnoreCase(data.getString("CATEGORY"))){
                customModelAlerts.setCategoryOrNot(false);
                customModelAlerts.setName(data.getString("DESCR"));
                customModelAlertses.add(customModelAlerts);
            }else{
                currentCategory = data.getString("CATEGORY");
                customModelAlerts1.setCategoryOrNot(true);
                customModelAlerts1.setName(data.getString("CATEGORY"));
                customModelAlertses.add(customModelAlerts1);

                customModelAlerts.setCategoryOrNot(false);
                customModelAlerts.setName(data.getString("DESCR"));
                customModelAlertses.add(customModelAlerts);
            }
            alertsModels.add(alertsModel);
        }

    }
}
