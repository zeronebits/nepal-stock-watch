package com.zeronebits.nepalstockwatch.alerts.view;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.alerts.model.AlertsModel;
import com.zeronebits.nepalstockwatch.alerts.model.CustomModelAlerts;
import com.zeronebits.nepalstockwatch.portfolio.model.IssueTypeModel;
import com.zeronebits.nepalstockwatch.portfolio.view.IssueTypeAdapter;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

/**
 * Created by Own on 12/4/2017.
 */

public class AlertsTypeAdapter extends ArrayAdapter<CustomModelAlerts> {

    LayoutInflater flater;
    String currentCategory ="";
    public AlertsTypeAdapter(Activity context, int resouceId, ArrayList<CustomModelAlerts> list){

        super(context,resouceId, list);
//        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return rowview(convertView,position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return rowview(convertView,position);
    }

    private View rowview(View convertView , int position){

        CustomModelAlerts rowItem = getItem(position);

        viewHolder holder ;
        View rowview = convertView;
        if (rowview==null) {

            holder = new viewHolder();
            flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = flater.inflate(R.layout.listitems_layout, null, false);

            holder.txtTitle = (TextView) rowview.findViewById(R.id.title);
            holder.txtHeader = (TextView) rowview.findViewById(R.id.tv_message_header);
            rowview.setTag(holder);
        }else{
            holder = (viewHolder) rowview.getTag();
        }
        holder.txtHeader.setText(rowItem.getName());
        holder.txtTitle.setText(rowItem.getName());
        if(rowItem.isCategoryOrNot()){
            holder.txtHeader.setVisibility(View.VISIBLE);
            holder.txtTitle.setVisibility(View.GONE);
        }else{
            holder.txtHeader.setVisibility(View.GONE);
            holder.txtTitle.setVisibility(View.VISIBLE);
        }

        return rowview;
    }

    private class viewHolder{
        TextView txtTitle,txtHeader;
    }
}