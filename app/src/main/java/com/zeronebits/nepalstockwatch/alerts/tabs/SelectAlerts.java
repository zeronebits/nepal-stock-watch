package com.zeronebits.nepalstockwatch.alerts.tabs;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;

import com.zeronebits.nepalstockwatch.alerts.model.AlertsModel;
import com.zeronebits.nepalstockwatch.alerts.model.CustomModelAlerts;
import com.zeronebits.nepalstockwatch.alerts.model.UserAlertsModel;
import com.zeronebits.nepalstockwatch.alerts.presenter.AlertsImp;
import com.zeronebits.nepalstockwatch.alerts.presenter.AlertsPresenter;
import com.zeronebits.nepalstockwatch.alerts.view.AlertsTypeAdapter;
import com.zeronebits.nepalstockwatch.alerts.view.AlertsView;
import com.zeronebits.nepalstockwatch.alerts.view.UserAlertsListAdapter;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyname.model.CompanyNameModel;
import com.zeronebits.nepalstockwatch.companyname.view.CompanyNameAdapter;
import com.zeronebits.nepalstockwatch.mainapplication.ApplicationMain;
import com.zeronebits.nepalstockwatch.portfolio.view.IssueTypeAdapter;
import com.zeronebits.nepalstockwatch.realmdatabase.RealmController;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.watchlist.model.WatchListModelDatabase;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Own on 12/3/2017.
 */

public class SelectAlerts extends BaseFragment implements AlertsView {

    AlertsPresenter alertsPresenter;
    @BindView(R.id.et_alerts_name)
    Spinner spinner;
    @BindView(R.id.et_watch_list_name)
    AutoCompleteTextView watchListName;
    ArrayList<WatchListModelDatabase> watchListUserModels;
    CompanyNameAdapter companyNameAdapter;
    int alertsName;
    ArrayList<AlertsModel> alertsModeles;
    @BindView(R.id.rv_user_alerts)
    RecyclerView recyclerView;
    ArrayList<AlertsModel> alertsModels;
    ArrayList<CustomModelAlerts> customModelAlertses;
    String categoryName;

    @Override
    protected int getLayout() {
        return R.layout.select_alerts;
    }

    @Override
    protected void init(View view) {
        initResume();
    }



    private void initResume() {
        alertsPresenter = new AlertsImp(this, getActivity());
        alertsPresenter.getAlertsList();
        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        ArrayList<CompanyNameModel> companyNameModels = realmController.getModelList();
        watchListName.setThreshold(1);
        watchListUserModels = realmController.getWatchList();
        Log.d("dfsdjnfsdf", watchListUserModels.size() + " ");
        companyNameAdapter = new CompanyNameAdapter(getActivity(), R.layout.simple_text_view, companyNameModels);
        watchListName.setAdapter(companyNameAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @OnClick(R.id.btn_watch_list_add)
    public void addAlerts() {
//        String issueAlerts = spinner.getSelectedItem().toString();
        String companyName = watchListName.getText().toString();

        if (companyName.isEmpty()) {
            watchListName.setError("Required");
        } else {
            if(getAlertsValue(categoryName)){
                alertsPresenter.addAlertsCategory(categoryName, companyName, Constant.USERCODE);
            }else{
                alertsPresenter.addAlerts(alertsName, companyName, Constant.USERCODE);
            }
        }
    }

    @Override
    public void alertsLists(ArrayList<AlertsModel> alertsModels, final ArrayList<CustomModelAlerts> customModelAlertses) {
        this.customModelAlertses = customModelAlertses;
        this.alertsModels = alertsModels;
        AlertsTypeAdapter alertsTypeAdapter = new AlertsTypeAdapter(getActivity(), R.layout.listitems_layout, customModelAlertses);
        spinner.setAdapter(alertsTypeAdapter);
        alertsPresenter.getUserAlertsList(Constant.USERCODE);
        this.alertsModeles = alertsModels;

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categoryName = customModelAlertses.get(position).getName();
//                alertsName = customModelAlertses.get(position).getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private boolean getAlertsValue(String categoryName) {
        boolean categoryOrNot = true;
        for(AlertsModel alertsModel : alertsModels){
            if(categoryName.equalsIgnoreCase(alertsModel.getdESCR())){
                categoryOrNot = false;
                alertsName = alertsModel.getcODE();
                break;
            }
        }
        return categoryOrNot;
    }

    @Override
    public void sucessfull() {
        alertsPresenter.getUserAlertsList(Constant.USERCODE);
    }

    @Override
    public void userAlertsList(ArrayList<UserAlertsModel> userAlertsModels) {
        UserAlertsListAdapter userAlertsListAdapter = new UserAlertsListAdapter(userAlertsModels,getActivity(),alertsModels,this);
        recyclerView.setAdapter(userAlertsListAdapter);
    }

    @Override
    public void deleteItem(String code, String desc) {
        Log.d("kdjfsdofhdsf", code + "   "+ desc);
        alertsPresenter.deleteAlerts(code,desc);

    }


}
