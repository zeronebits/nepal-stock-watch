package com.zeronebits.nepalstockwatch.alerts.presenter;

/**
 * Created by Own on 12/3/2017.
 */

public interface AlertsPresenter {
    void getAlertsList();

    void getUserAlertsList(String usercode);


    void addAlerts(int issueAlerts, String companyName, String usercode);

    void deleteAlerts(String code, String desc);

    void addAlertsCategory(String categoryName, String companyName, String usercode);
}
