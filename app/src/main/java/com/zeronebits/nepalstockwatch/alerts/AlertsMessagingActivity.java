package com.zeronebits.nepalstockwatch.alerts;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.zeronebits.nepalstockwatch.alerts.model.MessagingModel;
import com.zeronebits.nepalstockwatch.base.BaseActivity;
import com.zeronebits.nepalstockwatch.mainapplication.ApplicationMain;
import com.zeronebits.nepalstockwatch.realmdatabase.RealmController;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Own on 1/7/2018.
 */

public class AlertsMessagingActivity extends BaseActivity {


    ArrayList<MessagingModel> messagingModels;
    @BindView(R.id.rv_user_alerts)
    RecyclerView recyclerView;
    @BindView(R.id.ll_no_message)
    LinearLayout noMessage;
    @Override
    protected int getLayout() {
        return R.layout.alerts_message;
    }

    @Override
    protected void init() {
        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        messagingModels = realmController.getMessageList();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        if(messagingModels.size() > 0){
            AlertsMessageAdapter alertsMessageAdapter = new AlertsMessageAdapter(messagingModels,this);
            recyclerView.setAdapter(alertsMessageAdapter);
            noMessage.setVisibility(View.GONE);
        }else{
            noMessage.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }
}
