package com.zeronebits.nepalstockwatch.companyprofile.tab.companyhistory.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Own on 1/11/2018.
 */

public class CompanyDataModel {

    @SerializedName("DATE")
    @Expose
    private String dATE;
    @SerializedName("PRFL_CODE")
    @Expose
    private Integer pRFLCODE;
    @SerializedName("OPEN")
    @Expose
    private String oPEN;
    @SerializedName("HIGH")
    @Expose
    private String hIGH;
    @SerializedName("LOW")
    @Expose
    private String lOW;
    @SerializedName("CLOSE")
    @Expose
    private String cLOSE;
    @SerializedName("VOLUME")
    @Expose
    private Integer vOLUME;

    public String getdATE() {
        return dATE;
    }

    public void setdATE(String dATE) {
        this.dATE = dATE;
    }

    public Integer getpRFLCODE() {
        return pRFLCODE;
    }

    public void setpRFLCODE(Integer pRFLCODE) {
        this.pRFLCODE = pRFLCODE;
    }

    public String getoPEN() {
        return oPEN;
    }

    public void setoPEN(String oPEN) {
        this.oPEN = oPEN;
    }

    public String gethIGH() {
        return hIGH;
    }

    public void sethIGH(String hIGH) {
        this.hIGH = hIGH;
    }

    public String getlOW() {
        return lOW;
    }

    public void setlOW(String lOW) {
        this.lOW = lOW;
    }

    public String getcLOSE() {
        return cLOSE;
    }

    public void setcLOSE(String cLOSE) {
        this.cLOSE = cLOSE;
    }

    public Integer getvOLUME() {
        return vOLUME;
    }

    public void setvOLUME(Integer vOLUME) {
        this.vOLUME = vOLUME;
    }
}
