package com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Own on 10/24/2017.
 */

public class CompanyParser {
    String result;
    CompanyInfoModel companyInfoModel;

    public CompanyParser(String result) {
        this.result = result;
    }


    public CompanyInfoModel parser() throws JSONException {
        Log.d("dsndsfdf",result);
//        JSONArray jsonArray = new JSONArray(result);
        JSONObject data = new JSONObject(result);
         companyInfoModel = new CompanyInfoModel();

        companyInfoModel.setPrflTypeCOde(data.getString("PRFL_TYP_CODE"));
        companyInfoModel.setIndexCode(data.getString("INDEX_CODE"));
        companyInfoModel.setCode(data.getString("CODE"));
        companyInfoModel.setName(data.getString("NAME"));
        companyInfoModel.setDecr(data.getString("DESCR"));
        companyInfoModel.setAddr(data.getString("ADDR"));
        companyInfoModel.setEmail(data.getString("EMAIL"));
        companyInfoModel.setUrl(data.getString("URL"));
        companyInfoModel.setLtp(data.getString("LTP"));
        companyInfoModel.setChange(data.getString("CODE"));
        companyInfoModel.setTls(data.getString("TLS"));
        companyInfoModel.setPuv(data.getString("PDP_VAL"));
        companyInfoModel.setReverse(data.getString("RESERVE"));
        companyInfoModel.setEps(data.getString("EPS"));
        companyInfoModel.setPv(data.getString("PE"));
        companyInfoModel.setDiff(data.getString("DIFF"));
        companyInfoModel.setDiffP(data.getString("DIFFP"));
        companyInfoModel.setBookValue(data.getString("BOOK_VALUE"));

        return companyInfoModel;
    }
}
