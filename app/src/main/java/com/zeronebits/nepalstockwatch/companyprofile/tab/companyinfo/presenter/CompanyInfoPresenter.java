package com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.presenter;

/**
 * Created by Own on 10/24/2017.
 */

public interface CompanyInfoPresenter {
    void getCompanyInfo(String companyCode);
}
