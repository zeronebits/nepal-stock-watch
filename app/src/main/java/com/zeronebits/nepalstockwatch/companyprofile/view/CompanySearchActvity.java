package com.zeronebits.nepalstockwatch.companyprofile.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseActivity;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyname.model.CompanyNameModel;
import com.zeronebits.nepalstockwatch.companyname.view.CompanyNameAdapter;
import com.zeronebits.nepalstockwatch.mainapplication.ApplicationMain;
import com.zeronebits.nepalstockwatch.realmdatabase.RealmController;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Own on 10/10/2017.
 */

public class CompanySearchActvity extends BaseFragment {

    @BindView(R.id.btn_search)
    Button searchButton;
    @BindView(R.id.act_company_name)
    AutoCompleteTextView companySymbol;
    CompanyNameAdapter companyNameAdapter;
    ArrayList<CompanyNameModel> companyNameModels;
    MainActivity mainActivity;
    @Override
    protected int getLayout() {
        return R.layout.company_search;
    }

    @Override
    protected void init(View view) {
        RealmController realmController = RealmController.with(ApplicationMain.getContext()).getInstance();
        companyNameModels = realmController.getModelList();
        companyNameAdapter = new CompanyNameAdapter(getActivity(), R.layout.simple_text_view, companyNameModels);
        companySymbol.setThreshold(0);
        companySymbol.setAdapter(companyNameAdapter);
        companySymbol.requestFocus();
    }


    @OnClick(R.id.btn_search)
    public void searchCompany(){
        String companyNameString = companySymbol.getText().toString().trim();
        if(companyNameString.length() == 0){
            companySymbol.setError("Required");
        }else{
            Bundle b = new Bundle();
            Fragment fragment = new CompanyProfile();
            b.putString(Constant.COMPANYCODE, companyNameString);
            fragment.setArguments(b);

            if(Constant.USERCODE.equalsIgnoreCase("")){
                getFragmentManager().beginTransaction().replace(R.id.fl_activity_main_not_login, fragment).addToBackStack("tag").commit();
            }else{
                getFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame, fragment).addToBackStack("tag").commit();
            }
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        try {
            mainActivity = (MainActivity) getActivity();
            mainActivity.setToolBarTitle("Profile");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
