package com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.view;

import com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.model.CompanyNewsModel;

import java.util.ArrayList;

/**
 * Created by Own on 12/26/2017.
 */

public interface CompanyNewsView {
    void setCompanyNews(ArrayList<CompanyNewsModel> companyNewsModels);

    void noDataFound();
}
