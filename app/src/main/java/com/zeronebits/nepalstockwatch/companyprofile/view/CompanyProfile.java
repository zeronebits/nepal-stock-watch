package com.zeronebits.nepalstockwatch.companyprofile.view;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companydividend.view.CompanyDividend;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companyfloorsheet.view.CompanyFloorsheet;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companyhistory.view.CompanyHistoryFragment;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.view.CompanyInfo;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.view.CompanyNew;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalwatchstock.R;

import butterknife.BindView;

/**
 * Created by Own on 9/21/2017.
 */

public class CompanyProfile extends BaseFragment {

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    public static String companyCode;
    MainActivity mainActivity;
    @Override
    protected int getLayout() {
        return R.layout.compnay_profile;
    }

    @Override
    protected void init(View view) {
        companyCode = getArguments().getString(Constant.COMPANYCODE);
        Log.d("dklfnsdfnsdf",companyCode);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(0);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new CompanyInfo(), "Brief Info");
        adapter.addFragment(new CompanyHistoryFragment(), "History");
        adapter.addFragment(new CompanyFloorsheet(), "FloorSheet");
        adapter.addFragment(new CompanyDividend(), "Dividend");
        adapter.addFragment(new CompanyNew(), "News");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mainActivity = (MainActivity) getActivity();
            mainActivity.setToolBarTitle(companyCode);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
