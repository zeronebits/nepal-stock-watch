package com.zeronebits.nepalstockwatch.companyprofile.tab.companydividend.presenter;

import android.content.Context;
import android.util.Log;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companydividend.view.CompanyDividendView;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.portfolio.tabs.model.CompanyPortfolioParser;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 12/25/2017.
 */

public class CompanyDividendImpl implements CompanyDividendPresenter {

    CompanyDividendView companyDividendView;
    Context context;

    public CompanyDividendImpl(CompanyDividendView companyDividendView, Context context) {
        this.companyDividendView = companyDividendView;
        this.context = context;
    }

    @Override
    public void getDividendData(String companyCode) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getCompanyDividend(companyCode);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("totalGainFiscalYear", response.raw().request().url() + "  ");
                if (response.isSuccessful()) {
                    try {
                        CompanyPortfolioParser indexCodeParser = new CompanyPortfolioParser(response.body().string());
                        indexCodeParser.parser();
                        companyDividendView.companyTableData(indexCodeParser.headers, indexCodeParser.contentsWithValue);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

//                    watchListView.emptyWatchList();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }
}
