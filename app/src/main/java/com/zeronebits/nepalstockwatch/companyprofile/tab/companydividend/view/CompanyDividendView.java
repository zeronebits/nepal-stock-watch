package com.zeronebits.nepalstockwatch.companyprofile.tab.companydividend.view;

import java.util.ArrayList;

/**
 * Created by Own on 12/25/2017.
 */

public interface CompanyDividendView {
    void companyTableData(ArrayList<String> headers, ArrayList<ArrayList<String>> contentsWithValue);
}
