package com.zeronebits.nepalstockwatch.companyprofile.tab.companydividend.presenter;

/**
 * Created by Own on 12/25/2017.
 */

public interface CompanyDividendPresenter {
    void getDividendData(String companyCode);
}
