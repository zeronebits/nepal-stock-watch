package com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Own on 12/26/2017.
 */

public class CompanyNewsParser {
    String result;
    public ArrayList<CompanyNewsModel> companyNewsModels;

    public CompanyNewsParser(String result) {
        this.result = result;
        companyNewsModels = new ArrayList<>();
    }

    public void parser() throws JSONException {
        Log.d("fchgv",result);
        JSONArray jsonArray = new JSONArray(result);
        for(int i =0; i < jsonArray.length(); i++){
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            CompanyNewsModel companyNewsModel = new CompanyNewsModel();
            companyNewsModel.setNEWSFINAL(jsonObject.getString("NEWS_FINAL"));
            companyNewsModel.setPBLSHDDT(jsonObject.getString("PBLSHD_DT"));
            companyNewsModel.setURLSS(jsonObject.getString("URL_SS"));
            companyNewsModels.add(companyNewsModel);
        }
    }
}
