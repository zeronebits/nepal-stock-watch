package com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.view;

import com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.model.CompanyInfoModel;

/**
 * Created by Own on 10/24/2017.
 */

public interface CompanyInfoView {
    void setCompnayInfo(CompanyInfoModel companyInfoModel);

    void noDataFound();
}
