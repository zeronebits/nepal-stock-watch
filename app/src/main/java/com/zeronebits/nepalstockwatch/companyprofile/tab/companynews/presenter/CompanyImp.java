package com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.presenter;

import android.content.Context;
import android.util.Log;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.model.CompanyInfoModel;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.model.CompanyParser;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.model.CompanyNewsParser;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.view.CompanyNewsView;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 12/26/2017.
 */

public class CompanyImp implements CompanyNewsPresenter {

    CompanyNewsView companyNewsView;
    Context context;

    public CompanyImp(CompanyNewsView companyNewsView, Context context) {
        this.companyNewsView = companyNewsView;
        this.context = context;
    }

    @Override
    public void getNewsList(String companyCode) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getCompanyNews(companyCode);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("gcvgvjbj",response.code()+" ");
                if (response.isSuccessful()) {
                    try {
                        CompanyNewsParser companyParser = new CompanyNewsParser(response.body().string());
                        companyParser.parser();
                        companyNewsView.setCompanyNews(companyParser.companyNewsModels);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    companyNewsView.noDataFound();
//                    companyInfoView.emptyWatchList();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }
    }

