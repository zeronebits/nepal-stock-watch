package com.zeronebits.nepalstockwatch.companyprofile.tab.companyhistory.model;

import android.util.Log;

import com.github.mikephil.charting.data.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Own on 1/11/2018.
 */

public class CompanyDataParser {

    String result;

    public ArrayList<String> headers;
    ArrayList<String> contents;
    public ArrayList<ArrayList<String>> contentsWithValue;

    public ArrayList<String> chartData;
    public ArrayList<Entry> chartValue;


    public CompanyDataParser(String result) {
        this.result = result;
        headers = new ArrayList<>();
        chartData = new ArrayList<>();
        chartValue = new ArrayList<>();
        contents = new ArrayList<>();
        contentsWithValue = new ArrayList<>();
    }

    public void parser() throws JSONException {
        Log.d("jsdfdsbfdsf",result);

        JSONArray jsonArray = new JSONArray(result);
       /* for (int i=0; i < jsonArray.length(); i++) {
            JSONObject data = jsonArray.getJSONObject(i);
            CompanyDataModel companyDataModel = new CompanyDataModel();
            companyDataModel.setcLOSE(data.getString("CLOSE"));
            companyDataModel.setdATE(data.getString("DATE"));
            companyDataModel.sethIGH(data.getString("HIGH"));
            companyDataModel.setlOW(data.getString("LOW"));
            companyDataModel.setoPEN(data.getString("OPEN"));
            companyDataModel.setvOLUME(data.getInt("VOLUME"));
        }*/

        JSONArray data = new JSONArray(result);
       /* JSONArray data = jsonObject.getJSONArray("data");
        JSONArray headerJson = jsonObject.getJSONArray("header");
        for (int h = 0; h < headerJson.length(); h++) {
            headers.add(headerJson.getString(h));
        }*/

        headers.add("Date");
        headers.add("Open");
        headers.add("High");
        headers.add("Low");
        headers.add("Close");
        headers.add("Volume");

        for (int i = 0; i < data.length(); i++) {
            JSONObject jsonObject1 = data.getJSONObject(i);
            contents = new ArrayList<>();
            for (int a = 0; a < jsonObject1.length(); a++) {
                if (a == 1) {

                } else {
                    contents.add(jsonObject1.getString(jsonObject1.names().getString(a)));
                }
            }
            chartData.add(convertToApiFormat(jsonObject1.getString("DATE")));
            chartValue.add(new Entry(Float.parseFloat(i + ""), Float.parseFloat(jsonObject1.getString("VOLUME"))));
            contentsWithValue.add(contents);
        }
    }


    public String convertToApiFormat(String time) {
        String inputPattern = "yyyy-mm-dd";
        String outputPattern = "dd-MMM";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d(str, "checkdate");
        return str;
    }
}
