package com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Own on 12/26/2017.
 */

public class CompanyNewsModel {

    @SerializedName("NEWS_FINAL")
    @Expose
    private String nEWSFINAL;
    @SerializedName("PBLSHD_DT")
    @Expose
    private String pBLSHDDT;
    @SerializedName("URL_SS")
    @Expose
    private String uRLSS;

    public String getNEWSFINAL() {
        return nEWSFINAL;
    }

    public void setNEWSFINAL(String nEWSFINAL) {
        this.nEWSFINAL = nEWSFINAL;
    }

    public String getPBLSHDDT() {
        return pBLSHDDT;
    }

    public void setPBLSHDDT(String pBLSHDDT) {
        this.pBLSHDDT = pBLSHDDT;
    }

    public String getURLSS() {
        return uRLSS;
    }

    public void setURLSS(String uRLSS) {
        this.uRLSS = uRLSS;
    }

}
