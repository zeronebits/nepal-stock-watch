package com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.presenter;

import android.content.Context;
import android.util.Log;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.model.CompanyInfoModel;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.model.CompanyParser;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companyinfo.view.CompanyInfoView;
import com.zeronebits.nepalstockwatch.helper.LoadingHelper;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 10/24/2017.
 */

public class CompanyInfoImp implements CompanyInfoPresenter{

    CompanyInfoView companyInfoView;
    Context context;

    public CompanyInfoImp(CompanyInfoView companyInfoView, Context context) {
        this.companyInfoView = companyInfoView;
        this.context = context;
    }


    @Override
    public void getCompanyInfo(final String companyCode) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getCompanyInfo(companyCode);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("totalGain", response.raw().request().url() + "  ");

                if (response.isSuccessful()) {
                    try {
                        CompanyParser companyParser = new CompanyParser(response.body().string());
                        CompanyInfoModel companyInfoModel= companyParser.parser();
                        companyInfoView.setCompnayInfo(companyInfoModel);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                    e.printStackTrace();
                }

                } else {
                    companyInfoView.noDataFound();
//                    companyInfoView.emptyWatchList();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }
}
