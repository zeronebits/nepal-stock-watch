package com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.alerts.model.AlertsModel;
import com.zeronebits.nepalstockwatch.alerts.model.UserAlertsModel;
import com.zeronebits.nepalstockwatch.alerts.view.AlertsView;
import com.zeronebits.nepalstockwatch.companyprofile.tab.companynews.model.CompanyNewsModel;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Own on 12/26/2017.
 */

public class CompanyNewsApdater extends RecyclerView.Adapter<CompanyNewsApdater.ViewHolder> {

    ArrayList<CompanyNewsModel> companyNewsModels;
    Context context;

    public CompanyNewsApdater(ArrayList<CompanyNewsModel> companyNewsModels, Context context) {
        this.companyNewsModels = companyNewsModels;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.company_news_single, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CompanyNewsModel companyNewsModel = companyNewsModels.get(position);
        holder.content.setText(companyNewsModel.getNEWSFINAL());
        holder.date.setText(companyNewsModel.getPBLSHDDT());
    }


    @Override
    public int getItemCount() {
        return companyNewsModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_content)
        TextView content;
        @BindView(R.id.tv_date)
        TextView date;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}