package com.zeronebits.nepalstockwatch.userupdate.view;

/**
 * Created by Own on 1/23/2018.
 */

public interface UserUpdateView {
    void passwordChanged(String message);
}
