package com.zeronebits.nepalstockwatch.userupdate.view;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseActivity;
import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.helper.EditTextHelper;
import com.zeronebits.nepalstockwatch.login.model.UserModelForUpdate;
import com.zeronebits.nepalstockwatch.login.model.UserResponseModel;
import com.zeronebits.nepalstockwatch.splash.model.SplashParser;
import com.zeronebits.nepalstockwatch.userupdate.presenter.UserUpdateImp;
import com.zeronebits.nepalstockwatch.userupdate.presenter.UserUpdatePresenter;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalwatchstock.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Own on 1/23/2018.
 */

public class UserUpdateFragment extends BaseFragment implements UserUpdateView {

    MainActivity mainActivity;
    @BindView(R.id.et_full_name)
    EditText etFullName;
    @BindView(R.id.et_email_address)
    EditText etEmail;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_phone_number_primary)
    EditText etPhonePrimary;
    @BindView(R.id.et_phone_number_secondary)
    EditText etPhoneSecondary;
    @BindView(R.id.et_security)
    EditText etSecurity;
    @BindView(R.id.et_answer)
    EditText etAnswer;
    @BindView(R.id.et_pin)
    EditText etPin;
    @BindView(R.id.et_pin_confirm)
    EditText etPinConfirm;
    @BindView(R.id.et_password)
    EditText etNewPassword;
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;
    @BindView(R.id.et_old_password)
    EditText etOldPassword;

    UserUpdatePresenter userUpdatePresenter;
    UserResponseModel userResponseModel;

    @Override
    protected int getLayout() {
        return R.layout.user_update;
    }

    @Override
    protected void init(View view) {
        setDataToField();
        userUpdatePresenter = new UserUpdateImp(getActivity(), this);
    }

    private String getEditTextValue(EditText et) {
        return et.getText().toString();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mainActivity = (MainActivity) getActivity();
            mainActivity.setToolBarTitle("Settings");
        } catch (Exception e) {

        }
    }

    @OnClick(R.id.btn_change_pass)
    public void changePassword(){
        String oldPassword = getEditTextValue(etOldPassword).trim();
        String newPassword = getEditTextValue(etNewPassword).trim();
        String confirmNewPassword = getEditTextValue(etConfirmPassword).trim();

        if(oldPassword.isEmpty()){
            etOldPassword.setError("Required");
        }else if(newPassword.isEmpty()){
            etNewPassword.setError("Required");
        }else if(confirmNewPassword.isEmpty()){
            etConfirmPassword.setError("Required");
        }else if(!confirmNewPassword.equalsIgnoreCase(newPassword)){
            etConfirmPassword.setError("Incorrect Password");
        }else{
            userUpdatePresenter.changePassword(oldPassword,newPassword,confirmNewPassword);
        }
    }


    @OnClick(R.id.btn_submit)
    public void submitLogin() {
        String fullName = getEditTextValue(etFullName);
        String email = getEditTextValue(etEmail);
        String address = getEditTextValue(etAddress);
        String phonePrimary = getEditTextValue(etPhonePrimary);
        String phoneSecondary = getEditTextValue(etPhoneSecondary);
        String security = getEditTextValue(etSecurity);
        String answer = getEditTextValue(etAnswer);
        String password = getEditTextValue(etNewPassword);
        String confirmPassword = getEditTextValue(etConfirmPassword);
        String pin = getEditTextValue(etPin);
        String pinConfirm = getEditTextValue(etPinConfirm);

        UserModelForUpdate userModelForUpdate = new UserModelForUpdate(Constant.USERCODE, 0, 0,
                fullName, "", address, email, "", phoneSecondary, "");
        userUpdatePresenter.updateUser(userModelForUpdate);

    }


    public void setDataToField() {
        userResponseModel = SplashParser.userResponseModel;
        etFullName.setText(userResponseModel.getnAME());
        etEmail.setText(userResponseModel.geteMAIL());
        etAddress.setText(userResponseModel.getaDDR());
        etPhonePrimary.setText(userResponseModel.getcODE() + "");

        etPhoneSecondary.setText(userResponseModel.getaLTPHONE());
//        etSecurity.setText(userResponseModel.getnAME());
//        etFullName.setText(userResponseModel.getnAME());
//        etFullName.setText(userResponseModel.getnAME());
    }

    @Override
    public void passwordChanged(String message) {
        Toast.makeText(mainActivity, message, Toast.LENGTH_SHORT).show();
        etOldPassword.setText("");
        etNewPassword.setText("");
        etConfirmPassword.setText("");
    }
}
