package com.zeronebits.nepalstockwatch.userupdate.presenter;

import android.content.Context;
import android.util.Log;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.login.model.UserModelForUpdate;
import com.zeronebits.nepalstockwatch.splash.model.SplashParser;
import com.zeronebits.nepalstockwatch.userupdate.view.UserUpdateView;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.realm.CompactOnLaunchCallback;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 1/23/2018.
 */

public class UserUpdateImp implements UserUpdatePresenter {

    Context context;
    UserUpdateView userUpdateView;

    public UserUpdateImp(Context context, UserUpdateView userUpdateView) {
        this.context = context;
        this.userUpdateView = userUpdateView;
    }

    @Override
    public void updateUser(UserModelForUpdate userModelForUpdate) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();
        Log.d("CheckingCountryResponse", "i am here");

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.updateUserDetail(userModelForUpdate);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  ");
                if (response.isSuccessful()) {
                    try {
                        Log.d("45435", response.body().string());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else if(response.code() == 401 || response.code() == 409){
//                    loginView.sendVerification();
                } else {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
//                        loginView.failedLogin();
                    }catch (Exception e){

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void changePassword(String oldPassword, String newPassword, String confirmNewPassword) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(Constant.ACESS_TOKEN);
        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.changePassword(Constant.USERCODE,
                oldPassword,newPassword,confirmNewPassword);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse",response.code()+"    "+ response.raw().request().url() + "  ");
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String message = jsonObject.getString("message");
                        userUpdateView.passwordChanged(message);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else if(response.code() == 401 || response.code() == 409){
//                    loginView.sendVerification();
                } else {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
//                        loginView.failedLogin();
                    }catch (Exception e){

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }
}
