package com.zeronebits.nepalstockwatch.userupdate.presenter;

import com.zeronebits.nepalstockwatch.login.model.UserModelForUpdate;

/**
 * Created by Own on 1/23/2018.
 */

public interface  UserUpdatePresenter {
    void updateUser(UserModelForUpdate userModelForUpdate);

    void changePassword(String oldPassword, String newPassword, String confirmNewPassword);
}
