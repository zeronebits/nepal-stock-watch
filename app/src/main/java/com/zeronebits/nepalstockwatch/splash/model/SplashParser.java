package com.zeronebits.nepalstockwatch.splash.model;

import android.util.Log;

import com.zeronebits.nepalstockwatch.login.model.UserResponseModel;
import com.zeronebits.nepalstockwatch.utils.Constant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Own on 10/27/2017.
 */

public class SplashParser {


    String result;
    public String token;
    public static UserResponseModel userResponseModel;

    public SplashParser(String result) {
        this.result = result;
    }

    public void parser() throws JSONException {
        Log.d("cjsdfsdfujj",result);
        JSONObject jsonObject = new JSONObject(result);
        JSONObject userJson = jsonObject.getJSONObject("user");

        Constant.USERCODE = userJson.getString("CODE");
        userResponseModel = new UserResponseModel();
        try {
            token = jsonObject.getString("token");
        }catch (Exception e){
            e.printStackTrace();
        }

//        userResponseModel.setId(userJson.getInt("id"));
        userResponseModel.setnAME(userJson.getString("NAME"));
        userResponseModel.setpHOTOURL(userJson.getString("PHOTO_URL"));
        userResponseModel.setcODE(userJson.getInt("CODE"));
        userResponseModel.setaDDR(userJson.getString("ADDR"));
        userResponseModel.seteMAIL(userJson.getString("EMAIL"));
        userResponseModel.setdOB(userJson.getString("DOB"));
        userResponseModel.setdMATNO(userJson.getString("DMAT_NO"));
        userResponseModel.setaLTPHONE(userJson.getString("ALT_PHONE"));
        userResponseModel.setbLNC(userJson.getString("BLNC"));
    }
}