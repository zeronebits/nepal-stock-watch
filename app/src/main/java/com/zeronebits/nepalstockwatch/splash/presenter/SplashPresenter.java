package com.zeronebits.nepalstockwatch.splash.presenter;

/**
 * Created by Own on 10/17/2017.
 */

public interface SplashPresenter {
    void verifyLogin(String acessToken);

    void getCompanyIndexAndSubIndexDetail();
}
