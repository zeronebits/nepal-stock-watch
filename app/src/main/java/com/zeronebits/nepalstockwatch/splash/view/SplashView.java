package com.zeronebits.nepalstockwatch.splash.view;

/**
 * Created by Own on 10/17/2017.
 */

public interface SplashView {
    void verifySucess();

    void companyNameSucess();

    void failedLogin();
}
