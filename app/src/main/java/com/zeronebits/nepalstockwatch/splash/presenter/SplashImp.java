package com.zeronebits.nepalstockwatch.splash.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.companyname.model.CompanyInfoListParser;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.login.model.LoginParser;
import com.zeronebits.nepalstockwatch.login.view.LoginActivity;
import com.zeronebits.nepalstockwatch.mainactivitynotlogin.view.MainActivityNotLogin;
import com.zeronebits.nepalstockwatch.splash.model.SplashParser;
import com.zeronebits.nepalstockwatch.splash.view.SplashActivity;
import com.zeronebits.nepalstockwatch.splash.view.SplashView;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 10/17/2017.
 */

public class SplashImp implements SplashPresenter {

    SplashView splashView;
    Context context;

    public SplashImp(SplashView splashView, Context context) {
        this.splashView = splashView;
        this.context = context;
    }

    @Override
    public void verifyLogin(String acessToken) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofitWithAuthHeader(acessToken);
        Log.d("CheckingCountryResponse", "i am here");

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.verifyUser();
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  " + response.raw().request().url());
                if (response.isSuccessful()) {
                    try {
                        SplashParser loginParser = new SplashParser(response.body().string());
                        loginParser.parser();
                        splashView.verifySucess();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Constant.ACESS_TOKEN = "";
                        Intent i = new Intent(context, MainActivityNotLogin.class);
                        context.startActivity(i);
                        ((Activity) context).finish();
                        Log.d("sdkfjdsfsdf", response.errorBody().string());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getCompanyIndexAndSubIndexDetail() {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();
        Log.d("CheckingCountryResponse", "i am here");

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getCompanyList();
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("CheckingCountryResponse", response.code() + "  "+ response.raw().request().url());
                if (response.isSuccessful()) {
                    try {
//                        Log.d("sdkfjdsfsdf", response.body().string());
                        CompanyInfoListParser loginParser = new CompanyInfoListParser(response.body().string(),context);
                        loginParser.parser();
                        splashView.companyNameSucess();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Log.d("sdkfjdsfsdf", response.errorBody().string());
                        splashView.failedLogin();
                    }catch (Exception e){

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }
}
