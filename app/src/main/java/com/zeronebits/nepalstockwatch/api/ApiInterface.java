package com.zeronebits.nepalstockwatch.api;

import com.zeronebits.nepalstockwatch.login.model.LoginUserModel;
import com.zeronebits.nepalstockwatch.login.model.UserModelForUpdate;
import com.zeronebits.nepalstockwatch.portfolio.model.BuyStockModel;
import com.zeronebits.nepalstockwatch.portfolio.model.SellStockModel;
import com.zeronebits.nepalstockwatch.register.model.RegisterBody;
import com.zeronebits.nepalstockwatch.watchlist.model.WatchListModel;

import butterknife.BindDimen;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Own on 8/31/2017.
 */

public interface ApiInterface {

    @GET("nepse/data/performers/Top10ByGain.php")
    Call<ResponseBody> getDashboardTableData();

    @GET("public/api/v1/f-floorsheet")
    Call<ResponseBody> getFloorSheet(@Query("page") int page);

    @GET
    Call<ResponseBody> getFloorSheetNext(@Url String url);


    @GET("public/api/v1/livetrading")
    Call<ResponseBody> getLiveTrading();

    @GET("nepse/data/performers/Top10ByTurnOver.php")
    Call<ResponseBody> ApiGetTurnedOver();

    @GET("nepse/data/performers/Top10ByShareVolume.php")
    Call<ResponseBody> ApiGetShareTraded();

    @GET("nepse/data/performers/Top10ByTxn.php")
    Call<ResponseBody> ApiGetNoOfTransaction();

    @GET("nepse/data/performers/Top10ByLoss.php")
    Call<ResponseBody> ApiGetGain();

    @GET("nepse/data/performers/Top10ByLoss.php")
    Call<ResponseBody> ApiGetLoss();


    @POST("public/api/v1/register")
    Call<ResponseBody> createUser(@Body RegisterBody registerBody);


    @POST("public/api/v1/login")
    Call<ResponseBody> loginUser(@Body LoginUserModel loginUserModel);

    @GET("public/api/v1/validateLogin")
    Call<ResponseBody> verifyUser();

    @POST("public/api/v1/watch-list")
    Call<ResponseBody> addWatchList(@Body WatchListModel watchListModel);

    @GET("public/api/v1/watch-list")
    Call<ResponseBody> getWatchList(@Query("CSTM_CODE") String usercode);

    @GET("public/api/v1/company-profile")
    Call<ResponseBody> getCompanyInfo(@Query("CODE") String companyCode);

    @POST("public/api/v1/cstm-prfl-prtf-buy")
    Call<ResponseBody> addBuyStock(@Body BuyStockModel buyStockModel);

    @POST("public/api/v1/cstm-prfl-prtf-sell")
    Call<ResponseBody> addSellStock(@Body SellStockModel sellStockModel);

    @GET("public/api/v1/list-index")
    Call<ResponseBody> getCompanyList();

    @GET("public/api/v1/issu-typ")
    Call<ResponseBody> getIssuesType();

    @GET("public/api/v1/portfolio-by-company-profile")
    Call<ResponseBody> getPieChartDataCompany();

    @GET("public/api/v1/list-index")
    Call<ResponseBody> getIndexCode();

    @GET("public/api/v1/d-index")
    Call<ResponseBody> getDashboardChartData(@Query("INDEX_CODE") String indexCode,
                                             @Query("MONTH") int month);

    @GET("public/api/v1/portfolio-by-index-code")
    Call<ResponseBody> getPieChartDataIndex();

    @GET("public/api/v1/portfolio-by-index-code")
    Call<ResponseBody> getPieChart();

    @GET("public/api/v1/portfolio-by-issue-type-code")
    Call<ResponseBody> getPieIssueType();

    @GET("public/api/v1/delete/watch-list")
    Call<ResponseBody> deleteWatchList(@Query("PRFL_CODE") String s);

    @GET("public/api/v1/dashboard")
    Call<ResponseBody> getAllDashboardData();

    @GET("public/api/v1/bought-sold-share")
    Call<ResponseBody> getBoughtAndSoldShares(@Query("CSTM_CODE") String s);

    @GET("public/api/v1/portfolio-by-issue-type-code")
    Call<ResponseBody> getPieChartPortfolio();

    @GET("public/api/v1/portfolio-by-issue-type-code")
    Call<ResponseBody> getTotalGain(@Query("CSTM_CODE") String userCode);

    @GET("public/api/v1/prtf-hist")
    Call<ResponseBody> getProtfolio(@Query("CSTM_CODE") String usercode,@Query("MONTH") int i);

    @GET("public/api/v1/alrt")
    Call<ResponseBody> getAlertsList();

    @GET("public/api/v1/alrt-cstm")
    Call<ResponseBody> getUserAlertsList(@Query("CSTM_CODE") String usercode);

    @POST("public/api/v1/alrt-cstm-dtl")
    Call<ResponseBody> addAlertsUser(@Query("ALRT_CODE") int alrt,@Query("PRFL_CODE")
            String prfl,@Query("CSTM_CODE") String code);

    @GET("public/api/v1/f-floorsheet-by-prfl-code")
    Call<ResponseBody> getFloorSheetForCompany(@Query("PRFL_CODE") String companyCode);


    @GET("public/api/v1/alrt-delete")
    Call<ResponseBody> deleteAlerts(@Query("ALRT_CODE") String alrt,@Query("PRFL_CODE")
            String prfl,@Query("CSTM_CODE") String code);

    @GET("public/api/v1/f-dividend")
    Call<ResponseBody> getCompanyDividend(@Query("PRFL_CODE") String companyCode);

    @GET("public/api/v1/f-alrt-news")
    Call<ResponseBody> getCompanyNews(@Query("PRFL_CODE")String companyCode);

    @POST("public/api/v1/push-notification")
    Call<ResponseBody> sendToken(@Query("token")String token,@Query("CSTM_CODE") String usercode);

    @POST("public/api/v1/user/activate")
    Call<ResponseBody> verificationMobile(@Query("TOKEN")String verificationCode,@Query("code")
            String usercode);

    @POST("public/api/v1/resend-token")
    Call<ResponseBody> resendVerificationCode(@Query("code") String usercode);

    @GET("public/api/v1/d-index-by-profl")
    Call<ResponseBody> getCompanyData(@Query("PRFL_CODE") String selectedIndex,@Query("MONTH") int month);

    @POST("public/api/v1/alrt-cstm-dtl")
    Call<ResponseBody> addAlertsUserCategory(@Query("CATEGORY") String alrt,@Query("PRFL_CODE")
            String prfl,@Query("CSTM_CODE") String code);

    @POST("public/api/v1/forget-password")
    Call<ResponseBody> resetPassowrd(@Query("TOKEN")String confirmCode,@Query("password")
            String newPassword,@Query("code") String usercode);

    @POST("public/api/v1/user-update")
    Call<ResponseBody> updateUserDetail(@Body UserModelForUpdate userModelForUpdate);

    @POST("public/api/v1/change-password")
    Call<ResponseBody> changePassword(@Query("code") String code,@Query("old_password")String oldPassword,@Query("new_password") String newPassword,
                                      @Query("confirm_password")String confirmNewPassword);
}
