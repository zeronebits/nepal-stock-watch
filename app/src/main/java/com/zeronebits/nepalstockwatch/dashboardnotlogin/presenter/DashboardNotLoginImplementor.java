package com.zeronebits.nepalstockwatch.dashboardnotlogin.presenter;


import com.zeronebits.nepalstockwatch.dashboardnotlogin.model.Items;
import com.zeronebits.nepalstockwatch.dashboardnotlogin.view.DashboardNotLoginView;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

/**
 * Created by Own on 8/31/2017.
 */

public class DashboardNotLoginImplementor implements DashboardNotLoginPresenter {

    ArrayList<Items> items;
    DashboardNotLoginView mainActivityNotLoginView;

    public DashboardNotLoginImplementor(DashboardNotLoginView mainActivityNotLoginView) {
        this.mainActivityNotLoginView = mainActivityNotLoginView;
    }

    @Override
    public void getItems() {
        items = new ArrayList<>();
        items.add(new Items("\uf4b0","Dashboard", R.drawable.dashboard));
        items.add(new Items("\uf454","Floorsheet", R.drawable.floorsheet));
        items.add(new Items("\uf262","Live Trading", R.drawable.live_trading));
        items.add(new Items("\uf26c","Portfolio", R.drawable.portfolio));
        items.add(new Items("\uf133","Watchlist", R.drawable.watchlist));
        items.add(new Items("\uf19c","Company Profile", R.drawable.profile));
        items.add(new Items("\uf25f","Indices", R.drawable.indices));
        items.add(new Items("\uf26d","Calculator", R.drawable.calculator));
        items.add(new Items("\uf140","Support", R.drawable.support));
        mainActivityNotLoginView.setAdapter(items);
    }
}
