package com.zeronebits.nepalstockwatch.dashboardnotlogin.view;

import com.zeronebits.nepalstockwatch.dashboardnotlogin.model.Items;

import java.util.ArrayList;

/**
 * Created by Own on 8/31/2017.
 */

public interface DashboardNotLoginView {

    void setAdapter(ArrayList<Items> itemses);
}
