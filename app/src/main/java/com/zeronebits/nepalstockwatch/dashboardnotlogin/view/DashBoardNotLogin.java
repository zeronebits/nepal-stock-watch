package com.zeronebits.nepalstockwatch.dashboardnotlogin.view;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.zeronebits.nepalstockwatch.base.BaseFragment;
import com.zeronebits.nepalstockwatch.companyprofile.view.CompanySearchActvity;
import com.zeronebits.nepalstockwatch.dashboard.view.DashBoardFragment;
import com.zeronebits.nepalstockwatch.dashboardnotlogin.model.Items;
import com.zeronebits.nepalstockwatch.dashboardnotlogin.presenter.DashboardNotLoginImplementor;
import com.zeronebits.nepalstockwatch.dashboardnotlogin.presenter.DashboardNotLoginPresenter;
import com.zeronebits.nepalstockwatch.floorsheet.view.FloorSheetFragment;
import com.zeronebits.nepalstockwatch.indices.view.Indices;
import com.zeronebits.nepalstockwatch.livetrading.view.LiveTradingFragment;
import com.zeronebits.nepalstockwatch.login.view.LoginActivity;
import com.zeronebits.nepalstockwatch.mainactivitynotlogin.view.MainActivityNotLogin;
import com.zeronebits.nepalstockwatch.portfolio.view.PortfolioFragment;
import com.zeronebits.nepalwatchstock.R;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Own on 9/19/2017.
 */

public class DashBoardNotLogin extends BaseFragment implements DashboardNotLoginView {

    @BindView(R.id.gv_main_activity_list)
    GridView gridView;
    MainActivityNotLogin mainActivityNotLogin;
    @Override
    protected int getLayout() {
        return R.layout.dashboard_not_login;
    }

    @Override
    protected void init(View view) {
//        setToolbar();
        mainActivityNotLogin = (MainActivityNotLogin)getActivity();
        DashboardNotLoginPresenter mainActivityNotLoginPresenter = new DashboardNotLoginImplementor(this);
        mainActivityNotLoginPresenter.getItems();
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivityNotLogin.setToolBarTitle("Nepse");
        mainActivityNotLogin.hideHomeButton();
    }

    @Override
    public void setAdapter(ArrayList<Items> itemses) {
        Log.d("fjhdkasdasd", itemses.size() + "  ");
        DashboardItemAdapter mainActivityItemAdapter = new DashboardItemAdapter(getActivity(), R.layout.main_activity_item_adapter_single, itemses);
        gridView.setAdapter(mainActivityItemAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        setToolBarTitle("Dashboard");
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fl_activity_main_not_login, new DashBoardFragment(),"tag").
                                addToBackStack("tag").commit();
                        break;
                    case 1:
                        setToolBarTitle("Floorsheet");
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fl_activity_main_not_login,
                                        new FloorSheetFragment(),"tag").addToBackStack("tag").commit();
                        break;
                    case 2:
                        setToolBarTitle("Live Trading");
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fl_activity_main_not_login, new LiveTradingFragment(),"tag").
                                addToBackStack("tag").commit();
                        break;
                    case 3:
                        setToolBarTitle("Portfolio");
                        Intent b = new Intent(getActivity(), LoginActivity.class);
                        startActivity(b);
                        break;
                    case 4:
                        Intent a = new Intent(getActivity(), LoginActivity.class);
                        startActivity(a);
                        break;
                    case 5:
                        setToolBarTitle("Profile");
                        getFragmentManager().beginTransaction().replace(R.id.fl_activity_main_not_login,
                                new CompanySearchActvity(),"tag").addToBackStack("tag").commit();
                        break;
                    case 6:
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fl_activity_main_not_login, new Indices()).
                                addToBackStack("tag").commit();
                        break;
                    case 7:
                       /* setToolBarTitle("Calculator");
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fl_activity_main_not_login, new CalulatorFragment(),"tag").
                                addToBackStack("tag").commit();*/
                        break;
                    case 8:
                        Intent c = new Intent(getActivity(), LoginActivity.class);
                        startActivity(c);
                        break;
                }
            }
        });
    }

    private void setToolBarTitle(String dashboard) {
        mainActivityNotLogin.setToolBarTitle(dashboard);
    }


}
