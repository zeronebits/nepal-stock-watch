package com.zeronebits.nepalstockwatch.floorsheet.view;

import java.util.ArrayList;

/**
 * Created by Own on 9/2/2017.
 */

public interface FloorSheetView {

    void setAdapter(ArrayList<String> headers, ArrayList<ArrayList<String>> contents,String date);

    void symbolClicked(String s);

    void setNewAdapter(ArrayList<ArrayList<String>> tableContentsList);

    void errorLayout();
}
