package com.zeronebits.nepalstockwatch.floorsheet.presentar;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.zeronebits.nepalstockwatch.api.ApiInterface;
import com.zeronebits.nepalstockwatch.floorsheet.model.FloorSheetParser;
import com.zeronebits.nepalstockwatch.floorsheet.view.FloorSheetView;
import com.zeronebits.nepalstockwatch.helper.NoInternetConnectionHelper;
import com.zeronebits.nepalstockwatch.utils.SetupRetrofit;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Own on 9/2/2017.
 */

public class FloorSheetImplementor implements FloorSheetPresenter {

    FloorSheetView floorSheetView;
    ArrayList<ArrayList<String>> tableContentsList;
    ArrayList<String> headers;
    Context context;

    public FloorSheetImplementor(FloorSheetView floorSheetView, Context context) {
        this.floorSheetView = floorSheetView;
        this.context = context;
    }

    @Override
    public void getFloorSheetData(int page) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();
        Log.d("CheckingCountryResponse", "i am here");

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getFloorSheet(page);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("sdkjfsdklfjdkf", response.raw().request().url() + "  ");
                if(response.isSuccessful()) {
                    try {
//                    Log.d("CheckingCountryResponse", response.code() + "  ");
                        FloorSheetParser floorSheerParser = new FloorSheetParser(response.body().string());
                        floorSheerParser.parse();
                        tableContentsList = floorSheerParser.getContentsWithValue();
                        headers = floorSheerParser.getHeaders();
                        floorSheetView.setAdapter(headers, tableContentsList, floorSheerParser.date);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        floorSheetView.errorLayout();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void loadNextPage(String url) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();
        Log.d("CheckingCountryResponse", "i am here");

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getFloorSheetNext(url);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("sdkjfsdklfjdkf", response.raw().request().url() + "  ");
                if(response.isSuccessful()) {
                    try {
//                    Log.d("CheckingCountryResponse", response.code() + "  ");
                        FloorSheetParser floorSheerParser = new FloorSheetParser(response.body().string());
                        floorSheerParser.parse();
                        tableContentsList = floorSheerParser.getContentsWithValue();
                        headers = floorSheerParser.getHeaders();
                        floorSheetView.setNewAdapter(tableContentsList);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getFloorSheetDataofCompany(int page, String companyCode) {
        SetupRetrofit setupRetrofit = new SetupRetrofit();
        Retrofit retrofit = setupRetrofit.getRetrofit();
        Log.d("CheckingCountryResponse", "i am here");

        ApiInterface loginRegInterface = retrofit.create(ApiInterface.class);
        Call<ResponseBody> getCountry = loginRegInterface.getFloorSheetForCompany(companyCode);
        getCountry.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("sdkjfsdklfjdkf", response.raw().request().url() + "  ");
                if (response.isSuccessful()) {
                    try {
//                    Log.d("CheckingCountryResponse", response.code() + "  ");
                        FloorSheetParser floorSheerParser = new FloorSheetParser(response.body().string());
                        floorSheerParser.parse();
                        tableContentsList = floorSheerParser.getContentsWithValue();
                        headers = floorSheerParser.getHeaders();
                        floorSheetView.setAdapter(headers, tableContentsList, floorSheerParser.date);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                NoInternetConnectionHelper.showDialog(context);
                t.printStackTrace();
            }
        });
    }
}
