package com.zeronebits.nepalstockwatch.register.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeronebits.nepalstockwatch.base.BaseActivity;
import com.zeronebits.nepalstockwatch.login.view.LoginActivity;
import com.zeronebits.nepalstockwatch.register.model.RegisterBody;
import com.zeronebits.nepalstockwatch.register.presenter.RegisterImp;
import com.zeronebits.nepalstockwatch.register.presenter.RegisterPresenter;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalwatchstock.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Own on 8/27/2017.
 */

public class RegisterActivity extends BaseActivity implements RegisterView {

    @BindView(R.id.et_full_name)
    EditText etFullName;
    @BindView(R.id.et_email_address)
    EditText etEmailAddress;
    @BindView(R.id.et_mobile_no)
    EditText etMobileNo;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassowrd;
    @BindView(R.id.btn_register)
    Button registerButton;
    @BindView(R.id.tt_login)
    LinearLayout loginText;

    RegisterPresenter registerPresenter;

    @Override
    protected int getLayout() {
        return R.layout.register;
    }

    @Override
    protected void init() {
        registerPresenter = new RegisterImp(this, this);
    }


    @OnClick(R.id.btn_register)
    public void registerUser() {
        String name = etFullName.getText().toString();
        String code = etMobileNo.getText().toString();
        String email = etEmailAddress.getText().toString();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassowrd.getText().toString();

        if (name.length() == 0) {
            etFullName.setError("Required");
            etFullName.requestFocus();
        }else if (code.length() ==0){
            etMobileNo.setError("Required");
            etMobileNo.requestFocus();
            Constant.USERCODE = code;
        }else if(email.length() == 0){
            etEmailAddress.setError("Required");
            etEmailAddress.requestFocus();
        }else if(!isValidEmail(email)){
            etEmailAddress.setError("invalid email");
            etEmailAddress.requestFocus();
        }else if(password.length() == 0){
            etPassword.setError("Required");
            etPassword.requestFocus();
        }else if (code.length() < 10){
            etMobileNo.setError("Must be 10 digit");
            etMobileNo.requestFocus();
        }else if (!checkPhoneNumber(code)){
            etMobileNo.setError("invalid number");
            etMobileNo.requestFocus();
        }else if(confirmPassword.length() == 0) {
            etConfirmPassowrd.setError("Required");
            etConfirmPassowrd.requestFocus();
        }else if(!confirmPassword.equalsIgnoreCase(password)) {
            etConfirmPassowrd.setError("Password do not match");
            etConfirmPassowrd.requestFocus();
        }else{
            Constant.USERCODE = code;
            RegisterBody registerBody = new RegisterBody(name,code,email,password);
            registerPresenter.registerUser(registerBody);
        }

    }

    @OnClick(R.id.tt_login)
    public void loginUser(){
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public boolean isValidEmail(String email){
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean checkPhoneNumber(String number){
        char first = number.charAt(0);
        String value = first + "";
        if(value.equalsIgnoreCase("9")){
            return  true;
        }else{
            return false;
        }
    }



}
