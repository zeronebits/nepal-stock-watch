package com.zeronebits.nepalstockwatch.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.zeronebits.nepalstockwatch.activities.MainActivity;
import com.zeronebits.nepalstockwatch.base.BaseActivity;
import com.zeronebits.nepalstockwatch.forgotpassword.view.ForgotPassword;
import com.zeronebits.nepalstockwatch.helper.LoadingHelper;
import com.zeronebits.nepalstockwatch.helper.SharedPreferencesHelper;
import com.zeronebits.nepalstockwatch.login.model.LoginUserModel;
import com.zeronebits.nepalstockwatch.login.presenter.LoginImp;
import com.zeronebits.nepalstockwatch.login.presenter.LoginPresenter;
import com.zeronebits.nepalstockwatch.mainactivitynotlogin.view.MainActivityNotLogin;
import com.zeronebits.nepalstockwatch.register.view.RegisterActivity;
import com.zeronebits.nepalstockwatch.utils.Constant;
import com.zeronebits.nepalstockwatch.verification.view.VerificationActivity;
import com.zeronebits.nepalwatchstock.R;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Own on 8/27/2017.
 */

public class LoginActivity extends BaseActivity implements LoginView{

    LoginPresenter loginPresenter;
    @BindView(R.id.et_email_address) EditText username;
    @BindView(R.id.et_password) EditText password;
    @BindView(R.id.btn_login)
    Button login;
    @BindView(R.id.tv_register)
    TextView register;
    @BindView(R.id.tv_forgot)
    TextView forgotPassword;
    LoadingHelper loadingHelper;
    SharedPreferencesHelper sharedPreferencesHelper;
    @Override
    protected int getLayout() {
        return R.layout.login;
    }

    @Override
    protected void init() {
        loginPresenter = new LoginImp(this, this);
    }


    @OnClick(R.id.btn_login)
    public void loginClick(){
        String usernameString = username.getText().toString();
        String passwordString = password.getText().toString();

        Constant.USERCODE = usernameString;
        if(usernameString.length() == 0){
            username.setError("Required");
        }else if(passwordString.length() == 0){
            password.setError("Required");
        }else{
            loadingHelper = new LoadingHelper(this);
            loadingHelper.showDialog();
            sharedPreferencesHelper = SharedPreferencesHelper.getSharedInstance(this);
            sharedPreferencesHelper.saveUsernamePassword(usernameString,passwordString);
            LoginUserModel loginUserModel = new LoginUserModel(usernameString,passwordString);
            loginPresenter.loginUser(loginUserModel);
        }
    }

    @Override
    public void userToken(String token) {
        sharedPreferencesHelper = SharedPreferencesHelper.getSharedInstance(this);
        sharedPreferencesHelper.saveLoginPreferences(token);
        Constant.ACESS_TOKEN = token;
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void failedLogin() {
        if(loadingHelper.isShowing()){
            loadingHelper.disMiss();
        }
        Toast.makeText(this, "Incorrect credentials", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void companyNameSucess() {
        if(loadingHelper.isShowing()){
            loadingHelper.disMiss();
        }
    }

    @Override
    public void sendVerification() {
        if(loadingHelper.isShowing()){
            loadingHelper.disMiss();
        }
        Intent i = new Intent(this, VerificationActivity.class);
        startActivity(i);
    }


    @OnClick(R.id.tv_register)
    public void registerAccount() {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.tv_forgot)
    public void forgotPassword(){
        Intent intent = new Intent(LoginActivity.this, ForgotPassword.class);
        startActivity(intent);
        
    }
}
