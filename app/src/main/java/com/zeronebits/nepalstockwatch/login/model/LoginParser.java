package com.zeronebits.nepalstockwatch.login.model;

import android.util.Log;

import com.zeronebits.nepalstockwatch.utils.Constant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Own on 10/17/2017.
 */

public class LoginParser {


    String result;
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LoginParser(String result){
        this.result = result;
    }

    public void parser() throws JSONException {
        JSONObject jsonObject = new JSONObject(result);
        Log.d("sdfuhsdfdsf",result);
        token = jsonObject.getString("token");
        JSONObject data = jsonObject.getJSONObject("user");
        Constant.USERCODE = data.getString("CODE");

    }

}
