package com.zeronebits.nepalstockwatch.login.view;

/**
 * Created by Own on 10/16/2017.
 */

public interface LoginView {
    void userToken(String token);

    void failedLogin();

    void companyNameSucess();

    void sendVerification();
}
