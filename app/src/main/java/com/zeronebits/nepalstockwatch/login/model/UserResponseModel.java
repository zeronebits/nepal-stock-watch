package com.zeronebits.nepalstockwatch.login.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Own on 10/17/2017.
 */

public class UserResponseModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("MRCH_CODE")
    @Expose
    private Object mRCHCODE;
    @SerializedName("BROKER_CODE")
    @Expose
    private Object bROKERCODE;
    @SerializedName("NAME")
    @Expose
    private String nAME;
    @SerializedName("PHOTO_URL")
    @Expose
    private String pHOTOURL;
    @SerializedName("CODE")
    @Expose
    private Integer cODE;
    @SerializedName("ADDR")
    @Expose
    private String aDDR;
    @SerializedName("EMAIL")
    @Expose
    private String eMAIL;
    @SerializedName("REG_DT")
    @Expose
    private String rEGDT;
    @SerializedName("DMAT_NO")
    @Expose
    private String dMATNO;
    @SerializedName("ALT_PHONE")
    @Expose
    private String aLTPHONE;
    @SerializedName("DOB")
    @Expose
    private String dOB;
    @SerializedName("PIN")
    @Expose
    private int pIN;
    @SerializedName("TOKEN")
    @Expose
    private Integer tOKEN;
    @SerializedName("SQ_CODE")
    @Expose
    private String sQCODE;
    @SerializedName("SQ_ANS")
    @Expose
    private String sQANS;
    @SerializedName("BLNC")
    @Expose
    private String bLNC;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getmRCHCODE() {
        return mRCHCODE;
    }

    public void setmRCHCODE(Object mRCHCODE) {
        this.mRCHCODE = mRCHCODE;
    }

    public Object getbROKERCODE() {
        return bROKERCODE;
    }

    public void setbROKERCODE(Object bROKERCODE) {
        this.bROKERCODE = bROKERCODE;
    }

    public String getnAME() {
        return nAME;
    }

    public void setnAME(String nAME) {
        this.nAME = nAME;
    }

    public String getpHOTOURL() {
        return pHOTOURL;
    }

    public void setpHOTOURL(String pHOTOURL) {
        this.pHOTOURL = pHOTOURL;
    }

    public Integer getcODE() {
        return cODE;
    }

    public void setcODE(Integer cODE) {
        this.cODE = cODE;
    }

    public String getaDDR() {
        return aDDR;
    }

    public void setaDDR(String aDDR) {
        this.aDDR = aDDR;
    }

    public String geteMAIL() {
        return eMAIL;
    }

    public void seteMAIL(String eMAIL) {
        this.eMAIL = eMAIL;
    }

    public String getrEGDT() {
        return rEGDT;
    }

    public void setrEGDT(String rEGDT) {
        this.rEGDT = rEGDT;
    }

    public String getdMATNO() {
        return dMATNO;
    }

    public void setdMATNO(String dMATNO) {
        this.dMATNO = dMATNO;
    }

    public String getaLTPHONE() {
        return aLTPHONE;
    }

    public void setaLTPHONE(String aLTPHONE) {
        this.aLTPHONE = aLTPHONE;
    }

    public String getdOB() {
        return dOB;
    }

    public void setdOB(String dOB) {
        this.dOB = dOB;
    }

    public int getpIN() {
        return pIN;
    }

    public void setpIN(int pIN) {
        this.pIN = pIN;
    }

    public Integer gettOKEN() {
        return tOKEN;
    }

    public void settOKEN(Integer tOKEN) {
        this.tOKEN = tOKEN;
    }

    public String getsQCODE() {
        return sQCODE;
    }

    public void setsQCODE(String sQCODE) {
        this.sQCODE = sQCODE;
    }

    public String getsQANS() {
        return sQANS;
    }

    public void setsQANS(String sQANS) {
        this.sQANS = sQANS;
    }

    public String getbLNC() {
        return bLNC;
    }

    public void setbLNC(String bLNC) {
        this.bLNC = bLNC;
    }
}
